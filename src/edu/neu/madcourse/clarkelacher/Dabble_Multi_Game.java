package edu.neu.madcourse.clarkelacher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import edu.neu.mhealth.api.KeyValueAPI;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Dabble_Multi_Game extends Activity implements OnClickListener {
	AsyncTask<String, Void, String> updatewords;
	AsyncTask<String, Void, String> updatestatus;
	Button a1, a2, a3, b1, b2, b3, b4, c1, c2, c3, c4, c5, d1, d2, d3, d4, d5, d6; // Used to represent dabble letters
	Button pressed = null;
	ArrayList<String> FoundWords = new ArrayList<String>();
	public int Score = 0;
	boolean paused = false;
	CountDownTimer timer = null;
	long timeleft = 65000;
	//int checkTurn = 0;
	String word3 = "";
	String word4 = "";
	String word5 = "";
	String word6 = "";
	String aRow = "";
	String bRow = "";
	String cRow = "";
	String dRow = "";
	String timeString = "0";
	String scoreString = "0";
	String ID;
	MediaPlayer mp = new MediaPlayer();
	boolean muteFlag = true;
	boolean muteFlag2 = false;
	Vibrator vibrate;
	private Handler wordStore = new Handler();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		String nService = Context.NOTIFICATION_SERVICE;
		NotificationManager nManager = (NotificationManager) getSystemService(nService);
		nManager.cancel(223);
		updatestatus = new AsyncGameStatus().execute("Started");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble_game);
		
		wordStore.postDelayed(storeWords, 6000);
		
		Bundle extras = getIntent().getExtras();
		ID = getIntent().getStringExtra("gameID");
		//String ID = extras.getString("gameID");
		System.out.println("ID:" + ID);
		
		if (ID.equals("noID")){
		word3 = getaWord(3);
		word4 = getaWord(4);
		word5 = getaWord(5);
		word6 = getaWord(6);
		}
		else{
		word3 = extras.getString(ID + "word3");
		word4 = extras.getString(ID + "word4");
		word5 = extras.getString(ID + "word5");
		word6 = extras.getString(ID + "word6");
		}
		
		
		a1 = (Button) findViewById(R.id.buttonA1);
		a2 = (Button) findViewById(R.id.buttonA2);
		a3 = (Button) findViewById(R.id.buttonA3);
		b1 = (Button) findViewById(R.id.buttonB1);
		b2 = (Button) findViewById(R.id.buttonB2);
		b3 = (Button) findViewById(R.id.buttonB3);
		b4 = (Button) findViewById(R.id.buttonB4);
		c1 = (Button) findViewById(R.id.buttonC1);
		c2 = (Button) findViewById(R.id.buttonC2);
		c3 = (Button) findViewById(R.id.buttonC3);
		c4 = (Button) findViewById(R.id.buttonC4);
		c5 = (Button) findViewById(R.id.buttonC5);
		d1 = (Button) findViewById(R.id.buttonD1);
		d2 = (Button) findViewById(R.id.buttonD2);
		d3 = (Button) findViewById(R.id.buttonD3);
		d4 = (Button) findViewById(R.id.buttonD4);
		d5 = (Button) findViewById(R.id.buttonD5);
		d6 = (Button) findViewById(R.id.buttonD6);
		
		populateDabble();
		
		// Set up click listeners for all the buttons
	      a1.setOnClickListener(this);
	      a2.setOnClickListener(this);
	      a3.setOnClickListener(this);
	      b1.setOnClickListener(this);
	      b2.setOnClickListener(this);
	      b3.setOnClickListener(this);
	      b4.setOnClickListener(this);
	      c1.setOnClickListener(this);
	      c2.setOnClickListener(this);
	      c3.setOnClickListener(this);
	      c4.setOnClickListener(this);
	      c5.setOnClickListener(this);
	      d1.setOnClickListener(this);
	      d2.setOnClickListener(this);
	      d3.setOnClickListener(this);
	      d4.setOnClickListener(this);
	      d5.setOnClickListener(this);
	      d6.setOnClickListener(this);
	      
	      startTimer();
	      wordCheck();
	      vibrate = (Vibrator) Dabble_Multi_Game.this.getSystemService(Context.VIBRATOR_SERVICE);
	      Dabble_Music.play(this, R.raw.bloodred);
	}
	
	private Runnable storeWords = new Runnable() {
		   @Override
		   public void run() {
		      if (updatewords != null && !updatewords.isCancelled()){
				   System.out.println("Check A");
				   updatewords.cancel(true);
			   }
		      timeString = "" + timeleft;
			  scoreString = "" + Score;
		      
			  updatewords = new AsynchStoreWords().execute(aRow, bRow, cRow, dRow, timeString, scoreString);
			  System.out.println("Check AA");
			  // Stores the words every 6 seconds
		      wordStore.postDelayed(this, 6000);
		   }
		};
	
	protected void onResume() {
	      if (!muteFlag2){
		  muteFlag = true;
		  Dabble_Music.unmute(this);
	      }
		  super.onResume();
	   }

	protected void onPause() {
		super.onPause();	
		timer.cancel();
		TextView pause = (TextView) findViewById(R.id.pause);
		pause.setText("Resume");
		paused = true;
		Dabble_Music.pause(this);
		if(muteFlag){
	      Dabble_Music.mute(this);
	      }
	   }
	
	protected void onDestroy() {
		super.onDestroy();
		updatewords = new AsyncGameStatus().execute("Paused");
		if (updatewords != null && !updatewords.isCancelled()){
			   System.out.println("Check A");
			   updatewords.cancel(true);
		   }
		timeString = "" + timeleft;
		scoreString = "" + Score;
		
		updatewords = new AsynchStoreWords().execute(aRow, bRow, cRow, dRow, timeString, scoreString);
		timer.cancel();
		Dabble_Music.stop(this);
		wordStore.removeCallbacks(storeWords);
	}
	
	public void startTimer(){
		final TextView time = (TextView) findViewById(R.id.time);
		timer = new CountDownTimer(timeleft, 1000) {

		     public void onTick(long millisUntilFinished) {
		    	 timeleft = millisUntilFinished;    	 
		         long seconds = millisUntilFinished / 1000;
		         
		    	 time.setText("Seconds remaining: " + seconds);
		         
		         if (seconds < 8){
		        	 time.setTextColor(Color.RED);
		          	 time.setTypeface(null, Typeface.BOLD_ITALIC);
		         }
		         
		     }

		     public void onFinish() {
		    	 new AsyncGameStatus().execute("Lost");
		    	 lose();
		     }
		  };
		  timer.start();
	}
	
	public void pause(View view) throws InterruptedException{
		if (!paused){
			timer.cancel();
			Dabble_Music.pause(this);
			TextView pause = (TextView) findViewById(R.id.pause);
			pause.setText("Resume");
			paused = true;
		}
		else{
			startTimer();
			TextView pause = (TextView) findViewById(R.id.pause);
			pause.setText("Pause");
			Dabble_Music.resume(this);
			paused = false;
		}
	}
	
	public void mute(View view){
		Button mutebutton = (Button) findViewById(R.id.mute);
		String muteText = mutebutton.getText().toString();
		if (!paused){
		if (muteText.equals("Mute")){
			Dabble_Music.mute(this);
			muteFlag2 = true;
			mutebutton.setText("Unmute");
		}
		else{
			Dabble_Music.unmute(this);
			muteFlag2 = false;
			mutebutton.setText("Mute");
		}
		}
		
	}
	
	public void exit(View view){
		finish();
	}
	
	public void help(View view){
		muteFlag = false;
		Intent intent = new Intent(this, Dabble_Single_Help.class);
		intent.putExtra("word3", word3);
		intent.putExtra("word4", word4);
		intent.putExtra("word5", word5);
		intent.putExtra("word6", word6);
		startActivity(intent);
	}
	
	public void lose()
	{
		Intent intent = new Intent(this, Dabble_Multi_Defeat.class);
		intent.putExtra("ID", ID);
		intent.putExtra("Score", Score);
		wordStore.removeCallbacks(storeWords);
		finish();
		startActivity(intent);
	}
	
	public void populateDabble() {
		System.out.println("Populating Dabble");
		
		String mashup = word3 + word4 + word5 + word6;
		System.out.println("ID:" + ID);
		System.out.println("Scrambling Word:" + mashup);
		mashup = scrambleWord(mashup);
		System.out.println("Scrambled Word:" + mashup);
		
		a1.setText("" + mashup.charAt(0));
		a2.setText("" + mashup.charAt(1));
		a3.setText("" + mashup.charAt(2));
		b1.setText("" + mashup.charAt(3));
		b2.setText("" + mashup.charAt(4));
		b3.setText("" + mashup.charAt(5));
		b4.setText("" + mashup.charAt(6));
		c1.setText("" + mashup.charAt(7));
		c2.setText("" + mashup.charAt(8));
		c3.setText("" + mashup.charAt(9));
		c4.setText("" + mashup.charAt(10));
		c5.setText("" + mashup.charAt(11));
		d1.setText("" + mashup.charAt(12));
		d2.setText("" + mashup.charAt(13));
		d3.setText("" + mashup.charAt(14));
		d4.setText("" + mashup.charAt(15));
		d5.setText("" + mashup.charAt(16));
		d6.setText("" + mashup.charAt(17));
		System.out.println("Dabble populated");		
	}
	
	public String scrambleWord(String mashup){
		ArrayList<Character> mashed = new ArrayList<Character>();
		String scrambled = "";
		for (int y = 0; y < mashup.length(); y++){
			mashed.add(mashup.charAt(y));
		}
		Collections.shuffle(mashed);
		
		for (int x = 0; x < mashed.size(); x++){
			scrambled+= mashed.get(x);
		}
		
		return scrambled;
	}
	
	public String getaWord(int length){
		Random generator = new Random(System.currentTimeMillis());
		String[] Alphabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		int letterpick = generator.nextInt(26);
		String firstChar = Alphabet[letterpick];
		int wordpick = generator.nextInt(300); // "Magic" number that will work on any letter
		int count = 0;
		System.out.println("Looking for word of length:" + length);
		String dictRead = "";
		try{
            BufferedReader buff = new BufferedReader(new InputStreamReader(getAssets().open("words." + firstChar + ".txt")));
            
            	while ((dictRead = buff.readLine()) != null){
            		if (dictRead.length() == length){
				   if (count == wordpick){
					   System.out.println("Found:" + dictRead);
					   return dictRead;
				   }
				   count++;
            		}
				}
			buff.close();
			return getaWord(length); // If it reached here, it hit the end of the file without hitting the random number, so it tries again
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dictRead;
	}
	
	public void buttonSwap(Button b){
		vibrate.vibrate(80);
  	  if (pressed == null){
  		  pressed = b;
  		  b.setTypeface(null, Typeface.BOLD_ITALIC);
  		  b.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
  	  }
  	  else{
  		  CharSequence swapText = b.getText();
  		  b.setText(pressed.getText());
  		  pressed.setText(swapText);
  		  pressed.setTypeface(null, Typeface.NORMAL);
  		  pressed.setPaintFlags(pressed.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
  		  pressed = null;
  	  }
    }
	
	public void onClick(View view){
		if (!paused){
		Button b = (Button) findViewById(view.getId());
		buttonSwap(b);
		
		wordCheck();
		TextView scoreText = (TextView) findViewById(R.id.score);
		scoreText.setText("Score: " + Score);
		}
	}
	
	
	public void wordCheck(){
		boolean matchA = false;
		boolean matchB = false;
		boolean matchC = false;
		boolean matchD = false;
		aRow = a1.getText().toString() + a2.getText().toString() + a3.getText().toString();
		bRow = b1.getText().toString() + b2.getText().toString() + b3.getText().toString() + b4.getText().toString();
		cRow = c1.getText().toString() + c2.getText().toString() + c3.getText().toString() + c4.getText().toString() + c5.getText().toString();
		dRow = d1.getText().toString() + d2.getText().toString() + d3.getText().toString() + d4.getText().toString() + d5.getText().toString() + d6.getText().toString();
		
		timeString = "" + timeleft;
		//checkTurn++;
		scoreString = "" + Score;	
		
		matchA = isAWord(aRow);
		matchB = isAWord(bRow);
		matchC = isAWord(cRow);
		matchD = isAWord(dRow);
		
		if (matchA && matchB && matchC && matchD){
			new AsyncGameStatus().execute("Won");
			Intent intent = new Intent(this, Dabble_Multi_Victory.class);
			intent.putExtra("ID", ID);
			wordStore.removeCallbacks(storeWords);
			finish();
			startActivity(intent);
		}
		else{
			Score = 0;
		 if (matchA){
			 Score+= 3;
			 a1.setTextColor(Color.BLUE);
			 a2.setTextColor(Color.BLUE);
			 a3.setTextColor(Color.BLUE);
			 
			 if (!FoundWords.contains(aRow)){
				 MediaPlayer player = MediaPlayer.create(this,
				 android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
				 player.start();
				 FoundWords.add(aRow);
			 }
			 
				  
			 }
		 else{
			 a1.setTextColor(Color.BLACK);
			 a2.setTextColor(Color.BLACK);
			 a3.setTextColor(Color.BLACK); 
		 }
		 
		 if (matchB){
			 Score+=4;
			 b1.setTextColor(Color.BLUE);
			 b2.setTextColor(Color.BLUE);
			 b3.setTextColor(Color.BLUE);
			 b4.setTextColor(Color.BLUE);

			 if (!FoundWords.contains(bRow)){
				 MediaPlayer player = MediaPlayer.create(this,
				 android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
				 player.start();
				 FoundWords.add(bRow);
			 }
			 
			 }
		 else{
			 b1.setTextColor(Color.BLACK);
			 b2.setTextColor(Color.BLACK);
			 b3.setTextColor(Color.BLACK);
			 b4.setTextColor(Color.BLACK);
		 }
		 
		 if (matchC){
			 Score+=5;
			 c1.setTextColor(Color.BLUE);
			 c2.setTextColor(Color.BLUE);
			 c3.setTextColor(Color.BLUE);
			 c4.setTextColor(Color.BLUE);
			 c5.setTextColor(Color.BLUE);

			 if (!FoundWords.contains(cRow)){
				 MediaPlayer player = MediaPlayer.create(this,
				 android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
				 player.start();
				 FoundWords.add(cRow);
			 }
			 
			 }
		 else{
			 c1.setTextColor(Color.BLACK);
			 c2.setTextColor(Color.BLACK);
			 c3.setTextColor(Color.BLACK);
			 c4.setTextColor(Color.BLACK);
			 c5.setTextColor(Color.BLACK);
		 }
		 
		 if (matchD){
			 Score+=6;
			 d1.setTextColor(Color.BLUE);
			 d2.setTextColor(Color.BLUE);
			 d3.setTextColor(Color.BLUE);
			 d4.setTextColor(Color.BLUE);
			 d5.setTextColor(Color.BLUE);
			 d6.setTextColor(Color.BLUE);

			 if (!FoundWords.contains(dRow)){
				 MediaPlayer player = MediaPlayer.create(this,
				 android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
				 player.start();
				 FoundWords.add(dRow);
			 }
			 
			 }
		 else{
			 d1.setTextColor(Color.BLACK);
			 d2.setTextColor(Color.BLACK);
			 d3.setTextColor(Color.BLACK);
			 d4.setTextColor(Color.BLACK);
			 d5.setTextColor(Color.BLACK);
			 d6.setTextColor(Color.BLACK);
		 }
		 
		}
		 
		 }
	
	public boolean isAWord(String input){
		String firstChar = input.substring(0, 1);
		int result = 1;
		try{
            BufferedReader buff = new BufferedReader(new InputStreamReader(getAssets().open("words." + firstChar + ".txt")));
            String dictRead = "";
            	while ((dictRead = buff.readLine()) != null){
				   result = dictRead.compareToIgnoreCase(input);
				   if (result == 0){
					   buff.close();
					   return true;
				   }
				   if (result >  0){
					   buff.close();
					   return false;
				   }
				}
			buff.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dabble_game, menu);
		return true;
	}
private class AsynchStoreWords extends AsyncTask<String, Void, String>  {
		
	    @Override
	    protected String doInBackground(String... params) {
	    	
	    	while (true){
	    	int attempts = 0;
	    	// Attempts to ping the server every 6 seconds for 30 minutes
			while(attempts < 300 && !(KeyValueAPI.isServerAvailable())){
				attempts++;
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	    	
			if (KeyValueAPI.isServerAvailable()){
			
			/*
			if (params[0] == null){
				long storeTime = Long.valueOf(params[4]);
				System.out.println("Storing Time:" + storeTime);
				String gTime = ID + "-storedtime";
				KeyValueAPI.put("chlacher", "qwer1234", gTime, "" + storeTime);
				
				return null;
			} else{
			*/
	    	String storeWord3 = params[0];
	    	String storeWord4 = params[1];
	    	String storeWord5 = params[2];
	    	String storeWord6 = params[3];
	    	String storeTime = params[4];
	    	String storeScore = params[5];
	    	//int storeTurn = Integer.valueOf(params[5]);
	    	
	    	/*
	    	if (!ID.equals("noID")){
	    		String turnLoc = ID + "-turn";
		    	int compare = -1;
		    	String turnVal = KeyValueAPI.get("chlacher", "qwer1234", turnLoc);
		    	
		    	if (!turnVal.equalsIgnoreCase("Error: No Such Key")){
		    		try{
		    		compare = Integer.valueOf(turnVal);
		    		} catch (java.lang.NumberFormatException e){
		    			System.err.println(e);
		    			compare = -1;
		    		}
		    		
		    	}
	    	// In case multiple asynch tasks are going on after internet is restored, should store the highest turn value
	    	 
		    	if (storeTurn > compare){
		    */	
		    		String gTime = ID + "-storedtime";
		    		String gScore = ID + "-storedscore";
		    		String gWord3 = ID + "-storedword3";
		    		String gWord4 = ID + "-storedword4";
		    		String gWord5 = ID + "-storedword5";
		    		String gWord6 = ID + "-storedword6";
		    		
		    		KeyValueAPI.put("chlacher", "qwer1234", gScore, storeScore);
		    		KeyValueAPI.put("chlacher", "qwer1234", gTime, storeTime);
		    		KeyValueAPI.put("chlacher", "qwer1234", gWord3, storeWord3);
		    		KeyValueAPI.put("chlacher", "qwer1234", gWord4, storeWord4);
		    		KeyValueAPI.put("chlacher", "qwer1234", gWord5, storeWord5);
		    		KeyValueAPI.put("chlacher", "qwer1234", gWord6, storeWord6);
		    		
		    		return null;
	    	//}
	    	//}
			//}
			
			} else {
				System.err.println("This should not be happening");
				try {
					Thread.sleep(120000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
	    }
			
	    }        

	    @Override
	    protected void onPostExecute(String result) {
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}

	private class AsyncGameStatus extends AsyncTask<String, Void, String> {
	
    @Override
    protected String doInBackground(String... params) {
    	int attempts = 0;
    	// Attempts to ping the server every 6 seconds for 30 minutes
		while(attempts < 300 && !(KeyValueAPI.isServerAvailable())){
			attempts++;
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    	
		if (KeyValueAPI.isServerAvailable()){
		KeyValueAPI.put("chlacher", "qwer1234", ID + "-gameStatus", params[0]);		
	    }
		
		return null;    	
    }        

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}

}
