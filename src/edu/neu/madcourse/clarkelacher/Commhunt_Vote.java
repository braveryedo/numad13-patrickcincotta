package edu.neu.madcourse.clarkelacher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.mhealth.fileuploader.UploadAPI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Commhunt_Vote extends Activity {
ImageView photoImage = null;
Bitmap picture = null;
int numImages = 0;
int imageNumber = 1;
String gameID = null;
long servTime = -1;
String object = null;
CountDownTimer timer = null;
Button vote = null;
boolean pollFlag = true;
String commIDst = null;
Context context = null;
final String BaseUrl = "http://mhealth.ccs.neu.edu/PhoneAuthority/uploads/chlacher-";
//String imageString = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_commhunt_vote);
		photoImage = (ImageView) findViewById(R.id.photo_image);
		vote = (Button) findViewById(R.id.vote);
		//showPhoto(Uri.fromFile(getOutputPhotoFile()));
		Bundle extras = this.getIntent().getExtras();
		object = extras.getString("object");
		gameID = extras.getString("gameID");
		servTime = extras.getLong("servTime");
		commIDst = extras.getString("commIDst");
		
		context = getApplicationContext();
		
		Calendar c = Calendar.getInstance();
		long secondsSinceEpoch = c.getTimeInMillis();
		
		System.out.println("Download Checkpoint");
		String uploadPath = UploadAPI.download("chlacher", "qwer1234", gameID + "-" + commIDst + "-" + imageNumber);
		//String uploadPath = UploadAPI.download("chlacher", "qwer1234", gameID + "-" + object + "-" + imageNumber);
		uploadPath = uploadPath.substring(0, (uploadPath.length() - 1));
		System.out.println("Upload Path: " + uploadPath);
		
		long timeleft = (servTime + 180000) - secondsSinceEpoch;
		
		showImage(uploadPath);
		
		//showPhoto(Uri.fromFile(new File(UploadAPI.download("chlacher", "qwer1234", gameID + "image"))));
		//numImages = 4;
		View pageView = (View) findViewById(R.id.votepage);
		pageView.setOnTouchListener(new OnSwipeTouchListener() {
			public void onSwipeTop() {
		    }
		    public void onSwipeRight() {
		        cyclePhoto(1);
		    }
		    public void onSwipeLeft() {
		        cyclePhoto(-1);
		    }
		    public void onSwipeBottom() {
		    }
		});
		
		if (timeleft > 0){
			vote.setVisibility(View.GONE);
			startTimer(timeleft);
			new AsynchPollForImages().execute(false);			
		} else {
			new AsynchPollForImages().execute(true);
		}
	}
	
	public void showImage(String uploadPath){
		URL url = null;
		try {
			url = new URL(uploadPath);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InputStream content = null;
		try {
			content = (InputStream)url.getContent();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Drawable d = Drawable.createFromStream(content , "src"); 
		photoImage.setImageDrawable(d);
	}
	
	public void startTimer(long timeleft){
		final TextView timeview = (TextView) findViewById(R.id.timerText);
		System.out.println("Starting Timer");
    	timer = new CountDownTimer(timeleft, 1000) {

		     public void onTick(long millisUntilFinished) {
		    	 long seconds = millisUntilFinished / 1000;
		    	 timeview.setText("Seconds remaining: " + seconds);
		         
		         if (seconds < 8){
		        	 timeview.setTextColor(Color.RED);
		          	 timeview.setTypeface(null, Typeface.BOLD_ITALIC);
		         }
		         
		     }

		     public void onFinish() {
		    	 pollFlag = false;
		    	 timeview.setVisibility(View.GONE);
		     }
		  };
		  timer.start();
    	System.out.println("Timer started");
	}
	
	private void cyclePhoto(int offset){
		imageNumber += offset;
		imageNumber = imageNumber % numImages;
		
		if (imageNumber < 0){
			imageNumber += numImages;
		}
		
		System.out.println(imageNumber);
		
		String path = BaseUrl + gameID + "-" + commIDst + "-" + (imageNumber + 1);
		
		showImage(path);
		
		/*
		switch (imageNumber){
		case 0: {
			showPhoto(Uri.fromFile(getOutputPhotoFile()));
			break;
		}
		case 1: {
			Resources res = getResources();
		    Drawable d = res.getDrawable(R.drawable.samplea);
		    photoImage.setImageDrawable(d);
		    break;
		}
		case 2: {
			Resources res = getResources();
		    Drawable d = res.getDrawable(R.drawable.sampleb);
		    photoImage.setImageDrawable(d);
		    break;
		}
		case 3: {
			Resources res = getResources();
		    Drawable d = res.getDrawable(R.drawable.samplec);
		    photoImage.setImageDrawable(d);
		    break;
		}
		}
	*/
		
		
	}
	
	/* Old Code: Used to convert string back to file
	private File stringToImage(String s){
		try {
            // Converting a Base64 String into Image byte array
            byte[] imageByteArray = Base64.decode(s, Base64.DEFAULT); 
             
            File directory = new File(Environment.getExternalStoragePublicDirectory(
            		Environment.DIRECTORY_PICTURES), getPackageName());
            		    if (!directory.exists()) {
            		      if (!directory.mkdirs()) {
            		        System.out.println("Failed to create storage directory.");
            		        return null;
            		        }
            		    }
            
            String path = directory.getPath() + File.separator + "IMG_comHunt_save.jpg";
            // Write a image byte array into file system
            FileOutputStream imageOutFile = new FileOutputStream(path);
     
            imageOutFile.write(imageByteArray);
            imageOutFile.close();
            
            return new File(path);
        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
            return null;
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
            return null;
        }
 
    }
	*/
	private Bitmap decodeImage(File f) {
	      Bitmap bmp = null;
		  try {
		    BitmapFactory.Options opts = new BitmapFactory.Options();
		    opts.inJustDecodeBounds = true;
          FileInputStream fis = new FileInputStream(f);
		    BitmapFactory.decodeStream(fis, null, opts);
		    fis.close();

		    float sc = 0.0f;
		    int scale = 1;
		    if (opts.outHeight > opts.outWidth) {
		      sc = opts.outHeight / 1000;
		      scale = Math.round(sc);
		    } else {
		      sc = opts.outWidth / 1000;
		      scale = Math.round(sc);
		      }

		      BitmapFactory.Options o2 = new BitmapFactory.Options();
		      o2.inSampleSize = scale;
		      fis = new FileInputStream(f);
		      bmp = BitmapFactory.decodeStream(fis, null, o2);
		      fis.close();
		   } catch (IOException e) {
		       Log.e("IOException", e.toString());
		   }
		   return bmp;
		}
	
	public void voteOrDie(View view){
		Toast.makeText(this, "Voted for: " + (imageNumber + 1), Toast.LENGTH_SHORT).show();
	}
	/* Old Code: Used to get string data from server and draw it
	private class AsynchGetImage extends AsyncTask<String, Void, String> {
	    
    	protected String doInBackground(String... params) {	    	
    		String s = null;
    		int attempts = 0;		    		

    		System.out.println("Checking Internet Connection...");
    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
				attempts++;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
    			System.out.println("Pinging Server...");
			if (KeyValueAPI.isServerAvailable()){
				s = KeyValueAPI.get("chlacher", "qwer1234", "imageStore");
				System.out.println("Image Data Length On Vote: " + s.length());
				System.out.println("Success");	
		}
	}
    		return s;
    }

    @Override
    protected void onPostExecute(String result) {
    	System.out.println("Checkpoint A1");
    	if (result != null){
    		imageFile = stringToImage(result);
    		
    		ImageView photoImage = (ImageView) findViewById(R.id.photo_image);
    		System.out.println("Checkpoint A2");
    		if (imageFile.exists()){
    		      Bitmap bitmap = decodeImage(imageFile.getAbsoluteFile());
    		      BitmapDrawable drawable = new BitmapDrawable(getApplicationContext().getResources(), bitmap);
    		      photoImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
    		      photoImage.setImageDrawable(drawable);
    		    }
    	}
    	System.out.println("Checkpoint A3");
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
*/
	// In our next iteration, this method will take an input that will decide which locally stored image to load up
	// The images will be downloaded to the phone's internal storage at the beginning of this activity.
	private File getOutputPhotoFile() {
        File directory = new File(Environment.getExternalStoragePublicDirectory(
		Environment.DIRECTORY_PICTURES), getPackageName());
		return new File(directory.getPath() + File.separator + "IMG_comHunt.jpg");
	}
	  
	  private void showPhoto(Uri photoUri) {
		  System.out.println("Checkpoint 3");
		  if (photoUri != null){
		    File imageFile = new File(photoUri.getPath());
		    System.out.println("Checkpoint 4");
		    if (imageFile.exists()){
		    	
		      picture = decodeImage(imageFile.getAbsoluteFile());
		      System.out.println("Check 2");
		      
		      BitmapDrawable draw = new BitmapDrawable(this.getResources(), picture);
		      photoImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
		      photoImage.setImageDrawable(draw);
		    }
		  } else{
		    System.out.println("Fail Case");
		    /* Note:
		     * This is the catch case for an error that is now resolved.
		     * We will remove this once we are 100% positive that the error will not come back
		     */
		    Resources res = getResources();
		    Drawable d = res.getDrawable(R.drawable.photoicon);
		    //photoImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
		    photoImage.setImageDrawable(d);
		  }
		}
	  
	  
	  private class AsynchPollForImages extends AsyncTask<Boolean, Void, Integer> {
		    
	    	protected Integer doInBackground(Boolean... params) {
	    	   Boolean runOnce = params[0];
	    	   int attempts = 0;

	    	   System.out.println("Checking Internet Connection...");
	    	   ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    	   NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();
	    	   while (pollFlag){	    		   
	    		   while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
	    			   attempts++;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		   }
				
	    		   if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
	    			   System.out.println("Pinging Server...");
	    			   if (KeyValueAPI.isServerAvailable()){
							String number = KeyValueAPI.get("chlacher", "qwer1234", gameID + "-" + object + "-" + "number");
							numImages = Integer.valueOf(number);
							System.out.println("Number of Images: " + numImages);
							if (runOnce){
								pollFlag = false;
							} else {
								try {
									Thread.sleep(5000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							return 2; // Code Indicating KeyValueAPI server is not available
					}
	    		   } else {
	    			return 3; // Code Indicating Network is Unavailable
	    		   }
	    	   }
	    	   return 1;
	    	}
	    

	    @Override
	    protected void onPostExecute(Integer result) {
	    	if (result == 1){
	    		vote.setVisibility(View.VISIBLE);
	    	}
	    	if (result == 2){
	    		Toast.makeText(getApplicationContext(), "We're sorry, but it appears our servers are down. Please Try Again", Toast.LENGTH_SHORT).show();
	    	}
	    	if (result == 3){
	    		Toast.makeText(getApplicationContext(), "Your phone is not connected to the internet. Please find internet and try again", Toast.LENGTH_SHORT).show();
	    	}
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}
	  
	 protected void onDestroy(){
		 super.onDestroy();
		 pollFlag = false;
	 }

	
}
