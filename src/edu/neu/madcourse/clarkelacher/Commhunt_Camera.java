package edu.neu.madcourse.clarkelacher;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Locale;


import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.mhealth.fileuploader.UploadAPI;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Commhunt_Camera extends Activity {
    final int CAPTURE_IMAGE_ACTIVITY_REQ = 0;
    Locale locale;
	Uri fileUri = null;
	TextView currentObject = null;
	TextView currentState = null;
	Button photobutton = null;
	Button confirmbutton = null;
	TextView timeview = null;
	ImageView photoImage = null;
	String gameID = null;
	CountDownTimer timer = null;
	Bitmap picture = null;
	File imageFile = null;
	int imageNumber = -1;
	String object = null;
	long servTime = -1;
	String commIDst = null;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_commhunt_camera);
	    currentObject = (TextView) findViewById(R.id.currentObject);
	    photobutton = (Button) findViewById(R.id.button_callcamera);
	    confirmbutton = (Button) findViewById(R.id.button_confirm);
	    currentState = (TextView) findViewById(R.id.currentState);
	    timeview = (TextView) findViewById(R.id.time);
	    photoImage = (ImageView) findViewById(R.id.photo_image);
	    
	    currentState.setText("Case A: No Picture Yet");
	    photobutton.setText("Take Photo");
	    confirmbutton.setVisibility(View.GONE);
	    
	    gameID = getIntent().getStringExtra("gameID");
	    
	    new AsynchGetInfo().execute();
	  }
    
    public void onClickCamera(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = Uri.fromFile(getOutputPhotoFile());
        System.out.println("Stuff: " + fileUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQ );
      }
	  
    private File getOutputPhotoFile() {
        File directory = new File(Environment.getExternalStoragePublicDirectory(
		Environment.DIRECTORY_PICTURES), getPackageName());
		    if (!directory.exists()) {
		      if (!directory.mkdirs()) {
		        System.out.println("Failed to create storage directory.");
		        return null;
		        }
		    }
		return new File(directory.getPath() + File.separator + "IMG_comHunt.jpg");
	}
	  
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQ) {
		  if (resultCode == RESULT_OK) {
		    showPhoto(Uri.fromFile(getOutputPhotoFile()));		      
		  } else if (resultCode == RESULT_CANCELED) {
		      Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
		  } else {
		      Toast.makeText(this, "Callout for image capture failed!", Toast.LENGTH_LONG).show();
		    }
		  }
		}
	  
	  private void showPhoto(Uri photoUri) {
		  System.out.println("Checkpoint 3");
		  if (photoUri != null){
		    imageFile = new File(photoUri.getPath());
		    System.out.println("Checkpoint 4");
		    if (imageFile.exists()){
		    	
		      picture = decodeImage(imageFile.getAbsoluteFile());
		      System.out.println("Check 2");
		      
		      BitmapDrawable draw = new BitmapDrawable(this.getResources(), picture);
		      photoImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
		      photoImage.setImageDrawable(draw);
		      currentState.setText("Case B: Picture Taken");
		      photobutton.setText("Retake Photo");
		      confirmbutton.setVisibility(View.VISIBLE);
		    }
		  } else{
		    System.out.println("Fail Case");
		    /* Note:
		     * Occasionally, photoUri is null which was causing the app to crash
		     * In this event, we handle it by loading in a default image telling them to re-take the picture
		     * We have created an issue for this on bitbucket 
		     */
		    Resources res = getResources();
		    Drawable d = res.getDrawable(R.drawable.photoicon);
		    //photoImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
		    photoImage.setImageDrawable(d);
		    photobutton.setText("Retry");
		    currentState.setText("Case C: Picture Failed");
		    confirmbutton.setVisibility(View.GONE);
		  }
		}
	  
	  private Bitmap decodeImage(File f) {
	      Bitmap bmp = null;
		  try {
			System.out.println("Check 1");
		    BitmapFactory.Options opts = new BitmapFactory.Options();
		    opts.inJustDecodeBounds = true;
            FileInputStream fis = new FileInputStream(f);
            FileOutputStream fos;
		    BitmapFactory.decodeStream(fis, null, opts);
		    fis.close();
		    

		    float sc = 0.0f;
		    int scale = 1;
		    if (opts.outHeight > opts.outWidth) {
		      sc = opts.outHeight / 1000;
		      scale = Math.round(sc);
		    } else {
		      sc = opts.outWidth / 1000;
		      scale = Math.round(sc);
		      }

		      BitmapFactory.Options o2 = new BitmapFactory.Options();
		      o2.inSampleSize = scale;
		      fis = new FileInputStream(f);
		      bmp = BitmapFactory.decodeStream(fis, null, o2);
		      
		      fos = new FileOutputStream(f);
              bmp.compress(Bitmap.CompressFormat.JPEG, 10, fos);
              fos.flush();
              fos.close();
              
		      fis.close();
		   } catch (IOException e) {
		       Log.e("IOException", e.toString());
		   }
		   return bmp;
		}
	  
	  
	  /* Old code we were using to transform the image into a string
	  private String imageToString(File f) throws IOException {
		  Bitmap bm = BitmapFactory.decodeFile(f.getAbsolutePath());
		  ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		  bm.compress(Bitmap.CompressFormat.JPEG, 10, baos); //bm is the bitmap object   
		  byte[] b = baos.toByteArray();   
		  baos.close();
		  return Base64.encodeToString(b, Base64.DEFAULT);
		    }
	  */
	   
	   public void onClickConfirm(View view) {
		   System.out.println("Starting vote...");
		   new AsynchSendImage().execute(imageFile.getAbsolutePath());
		   /*
		   Intent intent = new Intent(getApplicationContext(), Commhunt_Vote.class);
	       intent.putExtra("gameID", gameID);
		   finish();
		   startActivity(intent);
		   */
	      }
	   
	   public void startTimer(long timeleft){
			
		}
	   /* Old Code: How we would send string data to the server (if it didn't truncate it)
	   private class AsynchSendImage extends AsyncTask<String, Void, String> {
		    
	    	protected String doInBackground(String... params) {	    	
	    		String s = params[0];
	    		int attempts = 0;		    		

	    		System.out.println("Checking Internet Connection...");
	    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

	    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
					attempts++;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
	    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
	    			System.out.println("Pinging Server...");
				if (KeyValueAPI.isServerAvailable()){
					KeyValueAPI.put("chlacher", "qwer1234", "imageStore", s);
					System.out.println("Success");	
			}
		}
	    		return null;
	    }

	    @Override
	    protected void onPostExecute(String result) {
	    	Intent intent = new Intent(getApplicationContext(), Commhunt_Vote.class);
			startActivity(intent);
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}
	   */
	   private class AsynchGetInfo extends AsyncTask<String, Void, String> {
		    
	    	protected String doInBackground(String... params) {	    	
	    		int attempts = 0;
	    		String retval = null;

	    		System.out.println("Checking Internet Connection...");
	    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

	    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
					attempts++;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
	    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
	    			System.out.println("Pinging Server...");
				if (KeyValueAPI.isServerAvailable()){
					System.out.println("Das ping");
					servTime = Long.valueOf(KeyValueAPI.get("chlacher", "qwer1234", "comHunt" + gameID + "startTime"));
					object = KeyValueAPI.get("chlacher", "qwer1234", "comHunt" + gameID + "object");
					commIDst = KeyValueAPI.get("chlacher", "qwer1234", "comHunt" + gameID + "commID");
					
					Calendar c = Calendar.getInstance();
					long secondsSinceEpoch = c.getTimeInMillis();
					
					long timeleft = (servTime + 180000) - secondsSinceEpoch;
					
					if (timeleft < 0){
						timeleft = 0;
					}
					
					retval = object + "-" + timeleft; 
					System.out.println("Success");	
			}
		}
	    		return retval;
	    }

	    @Override
	    protected void onPostExecute(String result) {
	    	String[] results = result.split("-");
	    	String object = results[0];
	    	long timeleft = Long.valueOf(results[1]);
	    	System.out.println("Starting Timer");
	    	timer = new CountDownTimer(timeleft, 1000) {

			     public void onTick(long millisUntilFinished) {
			    	 long seconds = millisUntilFinished / 1000;
			    	 timeview.setText("Seconds remaining: " + seconds);
			         
			         if (seconds < 8){
			        	 timeview.setTextColor(Color.RED);
			          	 timeview.setTypeface(null, Typeface.BOLD_ITALIC);
			         }
			         
			     }

			     public void onFinish() {
			    	 
			     }
			  };
			  timer.start();
	    	System.out.println("Timer started");
	    	currentObject.setText("Searching For: " + object);
	    	
	    	
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}
	   
	   private class AsynchSendImage extends AsyncTask<String, Void, Integer> {
		    
	    	protected Integer doInBackground(String... params) {
	    	   String path = params[0];
	    	   
	    	   System.out.println("Starting Upload Activity");
	    	   
	    	   System.out.println("Path: " + path);
	 		   /*
	    	   ByteArrayOutputStream baos = new ByteArrayOutputStream();
	 		   bmp.compress(Bitmap.CompressFormat.PNG, 10, baos);
	 		   //finish();
	 		   System.out.println("Starting Vote Activity 2");
	    	   byte[] retval = baos.toByteArray();
	    	   try {
	    		baos.flush();
				baos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			   */
	    	   int attempts = 0;

	    	   System.out.println("Checking Internet Connection...");
	    	   ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    	   NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

	    	   while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
					attempts++;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
	    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
	    		System.out.println("Pinging Server...");
					if (KeyValueAPI.isServerAvailable()){
						String number = KeyValueAPI.get("chlacher", "qwer1234", gameID + "-" + object + "-" + "number");						
						if (number.equalsIgnoreCase("Error: No Such Key")){
							imageNumber = 1;
						} else {
							imageNumber = Integer.valueOf(number) + 1;
						}
						KeyValueAPI.put("chlacher", "qwer1234", gameID + "-" + object + "-" + "number", "" + imageNumber);
						System.out.println("Uploading Image Number:" + imageNumber);
						System.out.println("Commercial ID :" + commIDst);
						UploadAPI.uploadFile("chlacher", "qwer1234", gameID + "-" + commIDst + "-" + imageNumber, path);
						System.out.println(UploadAPI.download("chlacher", "qwer1234", gameID + "-" + commIDst + "-" + imageNumber));
						return 1; // Code indicating success
					} else {
						return 2; // Code Indicating KeyValueAPI server is not available
					}
	    		} else
	    			return 3; // Code Indicating Network is Unavailable
		}
	    

	    @Override
	    protected void onPostExecute(Integer result) {
	    	if (result == 1){
	    		Intent intent = new Intent(getApplicationContext(), Commhunt_Vote.class);
	    		intent.putExtra("gameID", gameID);
	    		intent.putExtra("object", object);
	    		intent.putExtra("servTime", servTime);
	    		intent.putExtra("commIDst", commIDst);
	    		finish();
	    		startActivity(intent);
	    	}
	    	if (result == 2){
	    		Toast.makeText(getApplicationContext(), "We're sorry, but it appears our servers are down. Please Try Again", Toast.LENGTH_SHORT).show();
	    	}
	    	if (result == 3){
	    		Toast.makeText(getApplicationContext(), "Your phone is not connected to the internet. Please find internet and try again", Toast.LENGTH_SHORT).show();
	    	}
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}

}
