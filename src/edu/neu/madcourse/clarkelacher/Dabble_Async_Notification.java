package edu.neu.madcourse.clarkelacher;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import edu.neu.mhealth.api.KeyValueAPI;

public class Dabble_Async_Notification extends AsyncTask<String, Void, String> {
		Context context;
		NotificationManager nManager;
		PowerManager pm;
		PowerManager.WakeLock wl;
		Dabble_Async_Notification(Context cContext, NotificationManager cManager){
	    super();
	    context = cContext;
	    nManager= cManager;
		}    
	
	    protected String doInBackground(String... params) {
	    	pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
	    	wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "bckNoti");
	    	wl.acquire();
	  
	    	String ID = params[0];
	    	String aword3 = "";
	    	String aword4 = "";
	    	String aword5 = "";
	    	String aword6 = "";
	    	String targetGameNum = "" + (Integer.valueOf(params[1]) + 1);
	    	long timeleft = 0;
	    	int turns = 0;
	    	boolean newGame = false;
	    	boolean notifyresult = true;
	    	CharSequence nText = "You Have Been Challenged to Game ID: " + ID;
	    	
	    	while (true){
	    		
	    		while(KeyValueAPI.isServerAvailable()){
	    		// Polls the status of the given ID repeatedly until the status is "Pending"
	    		String checkStatus = KeyValueAPI.get("chlacher", "qwer1234", ID + "-gameStatus");
	    		String gameNum = KeyValueAPI.get("chlacher", "qwer1234", ID + "-gameNum");
	    		if ((checkStatus.equals("Challenge")) && (gameNum.equals(targetGameNum))){	    		
	    			String check = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword3");
	    			newGame = (check.equalsIgnoreCase("Error: No Such Key"));
	    			if (newGame){
	    				aword3 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word3");
	    				System.out.println("word3 -" + aword3);
	    				aword4 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word4");
	    				System.out.println("word4 -" + aword4);
	    				aword5 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word5");
	    				System.out.println("word5 -" + aword5);
	    				aword6 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word6");
	    				System.out.println("word6 -" + aword6);	
	    			} else{
	    				aword3 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword3");
	    				System.out.println("word3 -" + aword3);
	    				aword4 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword4");
	    				System.out.println("word4 -" + aword4);
	    				aword5 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword5");
	    				System.out.println("word5 -" + aword5);
	    				aword6 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword6");
	    				System.out.println("word6 -" + aword6);
	    				timeleft = Long.valueOf(KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedtime"));
	    				turns = Integer.valueOf(KeyValueAPI.get("chlacher", "qwer1234", ID + "-turn"));
	    				}
			
	    			if (newGame){
	    				return ID + "-" + aword3 + "-" + aword4 + "-" + aword5 + "-" + aword6 + "-" + nText;
	    			} else {
	    				return "RESUME" + "-" + ID + "-" + aword3 + "-" + aword4 + "-" + aword5 + "-" + aword6 + "-" + nText + "-" + timeleft + "-" + turns;
	    			}
	    				}
	    		
	    		if (notifyresult && (checkStatus.equals("Won"))){
	    			notifyresult = false;
	    			notify("Opponent has won Game ID: " + ID);
	    		}
	    		
	    		if (notifyresult && (checkStatus.equals("Lost"))){
	    			notifyresult = false;
	    			notify("Opponent has lost Game ID: " + ID);
	    		}
	    		
	    		try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		
	    		}
	    		// Waits 12 seconds before checking server status again if it is down
	    		try {
					Thread.sleep(12000);
					System.out.println("Sleeping");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	
	    	}
			
 	    }        

	    @Override
	    protected void onPostExecute(String result) {
	    	System.out.println(result);
	    	String[] parts = result.split("-");
	    	
	    	boolean resume;
	     	String ID;
	    	String aword3;
	    	String aword4;
	    	String aword5;
	    	String aword6;
	    	String nText;
	    	long timeleft;
	    	int turn;
	    	
	    	if (parts[0].equals("RESUME")){
	    	System.out.println("Resuming");
	    	resume = true;
	    	ID = parts[1];
	    	aword3 = parts[2];
	    	aword4 = parts[3];
	    	aword5 = parts[4];
	    	aword6 = parts[5];
	    	nText = parts[6];
	    	timeleft = Long.valueOf(parts[7]);
	    	turn = Integer.valueOf(parts[8]);
	    	System.out.println("Time left:" + timeleft);
	    	
	    	} else{
	    	resume = false;
	    	ID = parts[0];
	    	aword3 = parts[1];
	    	aword4 = parts[2];
	    	aword5 = parts[3];
	    	aword6 = parts[4];
	    	nText = parts[5];
	    	timeleft = -1;
	    	turn = -1;
	    	
	    	}
	    	
	    	long[] vibe = {0, 500, 250, 500 };
	    		    	
	    	NotificationCompat.Builder mBuilder =
				    new NotificationCompat.Builder(context)
				    .setSmallIcon(R.drawable.dabblenotification)
				    .setContentTitle("Dabble Game")
				    .setContentText(nText)
				    .setVibrate(vibe);
			
			Intent notificationIntent = null;
			if (resume){
				notificationIntent = new Intent(context,  Dabble_Multi_Game_Resume.class);
			}
			else{
				notificationIntent = new Intent(context,  Dabble_Multi_Game.class);
			}
			
			notificationIntent.removeExtra("gameID");
			notificationIntent.putExtra("gameID", ID);
			System.out.println(notificationIntent.getStringExtra("gameID"));
			System.out.println(ID);
			if (!ID.equals("noID")){
				notificationIntent.putExtra(ID + "word3", aword3);
				notificationIntent.putExtra(ID + "word4", aword4);
				notificationIntent.putExtra(ID + "word5", aword5);
				notificationIntent.putExtra(ID + "word6", aword6);
				if (resume){
				notificationIntent.putExtra(ID + "time", "" + timeleft);
				notificationIntent.putExtra(ID + "turn", "" + turn);
				System.out.println("Put Extra -" + aword3 + aword4 + aword5 + aword6 + timeleft + turn);
				}
				else{
				System.out.println("Put Extra -" + aword3 + aword4 + aword5 + aword6);
				}
			}
			PendingIntent pIntent = null;
			pIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			mBuilder.setContentIntent(pIntent);
			
			final int dabble_notification_ID = 223;
			nManager.notify(dabble_notification_ID, mBuilder.build());
			wl.release();	    	
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	    
	    void notify(String message){
	    	long[] vibe = {0, 250, 0, 250 };
	    	NotificationCompat.Builder mBuilder =
				    new NotificationCompat.Builder(context)
				    .setSmallIcon(R.drawable.dabblenotification)
				    .setContentTitle("Dabble Game")
				    .setContentText(message)
				    .setVibrate(vibe);
	    	
	    	final int dabble_notification_ID = 224;
			nManager.notify(dabble_notification_ID, mBuilder.build());
	    }
	}