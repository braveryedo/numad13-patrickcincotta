package edu.neu.madcourse.clarkelacher;
// Note: This code was taken from the Sudoku_Activity Project
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;

public class Dabble_Music {
	   private static MediaPlayer mp = null;

	   /** Stop old song and start new one */
	   
	   public static void play(Context context, int resource) {
	      stop(context);

	      // Start music only if not disabled in preferences
	      if (Sudoku_Prefs.getMusic(context)) {
	         mp = MediaPlayer.create(context, resource);
	         mp.setLooping(true);
	         mp.start();
	         
	      }
	   }
	   

	   /** Stop the music */
	   public static void stop(Context context) { 
	      if (mp != null) {
	         mp.stop();
	         mp.release();
	         mp = null;
	      }
	   }
	   
	   public static void pause(Context context){
		   if (mp != null) {
			   mp.pause();
		   }
	   }
	   
	   public static void resume(Context context){
		   if (mp != null) {
			   mp.start();
		   }
	   }
	   
	   public static void mute(Context context){
		   if (mp != null){
		   mp.setVolume(0, 0);
		   }
	   }
	   
	   public static void unmute (Context context){
		   if (mp != null){
		   mp.setVolume(1.0F, 1.0F);
		   }
	   }
	}
