package edu.neu.madcourse.clarkelacher;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class Dabble_Single_Help extends Activity {
Button a1, a2, a3, b1, b2, b3, b4, c1, c2, c3, c4, c5, d1, d2, d3, d4, d5, d6; // Used to represent dabble letters
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble_help);
		
		a1 = (Button) findViewById(R.id.buttonA1h);
		a2 = (Button) findViewById(R.id.buttonA2h);
		a3 = (Button) findViewById(R.id.buttonA3h);
		b1 = (Button) findViewById(R.id.buttonB1h);
		b2 = (Button) findViewById(R.id.buttonB2h);
		b3 = (Button) findViewById(R.id.buttonB3h);
		b4 = (Button) findViewById(R.id.buttonB4h);
		c1 = (Button) findViewById(R.id.buttonC1h);
		c2 = (Button) findViewById(R.id.buttonC2h);
		c3 = (Button) findViewById(R.id.buttonC3h);
		c4 = (Button) findViewById(R.id.buttonC4h);
		c5 = (Button) findViewById(R.id.buttonC5h);
		d1 = (Button) findViewById(R.id.buttonD1h);
		d2 = (Button) findViewById(R.id.buttonD2h);
		d3 = (Button) findViewById(R.id.buttonD3h);
		d4 = (Button) findViewById(R.id.buttonD4h);
		d5 = (Button) findViewById(R.id.buttonD5h);
		d6 = (Button) findViewById(R.id.buttonD6h);
		
		Bundle extras = getIntent().getExtras();
		String word3 = extras.getString("word3");
		String word4 = extras.getString("word4");
		String word5 = extras.getString("word5");
		String word6 = extras.getString("word6");
		
		a1.setText("" + word3.charAt(0));
		a2.setText("" + word3.charAt(1));
		a3.setText("" + word3.charAt(2));
		b1.setText("" + word4.charAt(0));
		b2.setText("" + word4.charAt(1));
		b3.setText("" + word4.charAt(2));
		b4.setText("" + word4.charAt(3));
		c1.setText("" + word5.charAt(0));
		c2.setText("" + word5.charAt(1));
		c3.setText("" + word5.charAt(2));
		c4.setText("" + word5.charAt(3));
		c5.setText("" + word5.charAt(4));
		d1.setText("" + word6.charAt(0));
		d2.setText("" + word6.charAt(1));
		d3.setText("" + word6.charAt(2));
		d4.setText("" + word6.charAt(3));
		d5.setText("" + word6.charAt(4));
		d6.setText("" + word6.charAt(5));
	}
	
	public void exit(View view){
		finish();
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return true;
	}

}
