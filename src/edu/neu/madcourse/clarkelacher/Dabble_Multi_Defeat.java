package edu.neu.madcourse.clarkelacher;

import edu.neu.mhealth.api.KeyValueAPI;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Dabble_Multi_Defeat extends Activity {
String ID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble_defeat);
		
		Bundle extras = getIntent().getExtras();
		int Score = extras.getInt("Score");
		ID = extras.getString("ID");
		
		final TextView score = (TextView) findViewById(R.id.scoreboard);
		score.setText("You Ran Out of Time.\nFinal Score: " + Score);
		
		final Button button1 = (Button) findViewById(R.id.button1);
		button1.setText("Challenge Opponent");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.defeat, menu);
		return true;
	}
	
	public void newGame(View view){
		new AsynchNewGame().execute(ID);
	}
	
	public void exitButton(View view){
		finish();
	}

	private class AsynchNewGame extends AsyncTask<String, Void, String> {
    	protected String doInBackground(String... params) {	    	
    		String ID = params[0];
    		int attempts = 0;
			while(attempts < 100 && !(KeyValueAPI.isServerAvailable())){
				attempts++;
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (KeyValueAPI.isServerAvailable()){
			
			KeyValueAPI.clearKey("chlacher", "qwer1234", ID);
			KeyValueAPI.clearKey("chlacher", "qwer1234", ID + "-storedword3");	
			}
		
			else{
				System.out.println("Server Unavailable");
			}
		
		return null;
	    }        

    @Override
    protected void onPostExecute(String result) {
    	Intent intent = new Intent(getApplicationContext(), Dabble_Multi_Create.class);
		intent.putExtra("Challenge", true);
		intent.putExtra("ID", ID);
		finish();
		startActivity(intent);   
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}

}

