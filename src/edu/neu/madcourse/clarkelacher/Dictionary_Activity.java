package edu.neu.madcourse.clarkelacher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Dictionary_Activity extends Activity {
String found = "";
HashSet<String> foundWords = new HashSet<String>();
@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dictionary);		
		
		EditText editText = (EditText) findViewById(R.id.edit_message);
		editText.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	onKey();
	        }

	    });

		
		
	}
	
	public void onKey(){
		 EditText editText = (EditText) findViewById(R.id.edit_message);
		 String message = editText.getText().toString();
		 boolean match = false;
		 if (message.length() >= 3){
		 match = isAWord(message);
		 }		 
		 
		 final TextView isWord = (TextView) findViewById(R.id.truefalse);
		 if (match){  
			 if (!foundWords.contains(message)){
				 MediaPlayer player = MediaPlayer.create(this,
						    android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
						player.start(); 
				found = message + "\n" + found;}
			 	foundWords.add(message);
		 }
		 isWord.setText(found);
	}

	public boolean isAWord(String input){
		String firstChar = input.substring(0, 1);
		int result = 1;
		try{
            BufferedReader buff = new BufferedReader(new InputStreamReader(getAssets().open("words." + firstChar + ".txt")));
            String dictRead = "";
            	while ((dictRead = buff.readLine()) != null){
				   result = dictRead.compareToIgnoreCase(input);
				   if (result == 0){
					   buff.close();
					   return true;
				   }
				   if (result >  0){
					   buff.close();
					   return false;
				   }
				}
			buff.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dictionary, menu);
		return true;
	}
	
	public void clearList(View view){
		final TextView isWord = (TextView) findViewById(R.id.truefalse);
		EditText editText = (EditText) findViewById(R.id.edit_message);
		found = "";
		foundWords.clear();
		isWord.setText(found);
		editText.setText(found);
	}
	
	public void backButton(View view) {
    	finish();
	}
	
	public void acknButton(View view) {
		Intent intent = new Intent(this, Main_Acknowledgements.class);
		startActivity(intent);
	}
	
}
