package edu.neu.madcourse.clarkelacher;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import edu.neu.mobileClass.*;

public class Main_Activity extends Activity {
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	
	protected void onCreate(Bundle savedInstanceState) {
		PhoneCheckAPI.doAuthorization(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void aboutButton(View view) {
		// Gets the service attributes from the context
		String ts = Context.TELEPHONY_SERVICE;
		// Extracts imei value from service attributes
        TelephonyManager TelephonyMgr = (TelephonyManager) getSystemService(ts);
        String imei = TelephonyMgr.getDeviceId();
         
        Intent intent = new Intent(this, Main_About.class);
		// Includes the imei as an additional message
		intent.putExtra(EXTRA_MESSAGE, imei); 
		startActivity(intent);		
	}
	
	public void sudokuButton(View view) {
		Intent intent = new Intent(this, Sudoku_Activity.class);
		startActivity(intent);
	}
	
	public void dabbleButton(View view) {
		Intent intent = new Intent(this, Dabble_Activity.class);
		startActivity(intent);
	}
	
	public void commButton(View view) {
		Intent intent = new Intent(this, Dabble_Multi_Activity.class);
		intent.putExtra("WordFlag", 0);
		startActivity(intent);
	}
	
	public void dictButton(View view) {
		Intent intent = new Intent(this, Dictionary_Activity.class);
		startActivity(intent);
	}
	
	public void trickButton(View view){
		Intent intent = new Intent(this, Commhunt_Start.class);
		startActivity(intent);
	}
	
	public void crashButton(View view) {
	    // Results in an Array out of Bounds Exception
		int[] x = {1, 2, 3, 4, 5, 6};
		int count = 0;
	    for (int y = 0; y <= x.length; y++) {
	    	count++;
	    	x[y] += count;
	  	}
	}
	
	public void exitButton(View view){
		finish();
	}

}
