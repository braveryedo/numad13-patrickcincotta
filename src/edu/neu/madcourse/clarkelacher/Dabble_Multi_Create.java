package edu.neu.madcourse.clarkelacher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Dabble_Multi_Create extends Activity {
	int letternum;
	String ID;
	String found;
	String word3;
	String word4;
	String word5;
	String word6;
	boolean challenge = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		letternum = 3;
		found = "";
		word3 = "";
		word4 = "";
		word5 = "";
		word6 = "";
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble_multi_create);
		
		Bundle extras = getIntent().getExtras();
		ID = extras.getString("ID");
		challenge = extras.getBoolean("Challenge");
		
		EditText editText = (EditText) findViewById(R.id.edit_message_dabble);
		editText.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	onKey();
	        }

	    });

		
		
	}
	
	public void onKey(){
		 EditText editText = (EditText) findViewById(R.id.edit_message_dabble);
		 Button confirm = (Button) findViewById(R.id.button2);
		 String message = editText.getText().toString();
		 boolean match = false;
		 if (message.length() == letternum && letternum == 3){
			 match = isAWord(message);
			 word3 = message;
			 editText.setHint("Enter a 4 letter word");
		 	}
		 
		 if (message.length() == letternum && letternum == 4){
			 match = isAWord(message);
			 word4 = message;
			 editText.setHint("Enter a 5 letter word");
			 }
		 
		 if (message.length() == letternum && letternum == 5){
			 match = isAWord(message);
			 word5 = message;
			 editText.setHint("Enter a 6 letter word");
			 }
		 
		 if (message.length() == letternum && letternum == 6){
			 match = isAWord(message);
			 word6 = message;
			 editText.setHint("Done");
			 editText.clearFocus();
			 if (match){
			 confirm.setText("Confirm");
			 }
			 }
		 
		 final TextView isWord = (TextView) findViewById(R.id.dabble_Words);
		 if (match){ 
			 found = word3 + "\n" + word4 + "\n" + word5 + "\n" + word6;
			 isWord.setText(found);
			 letternum++;
			 editText.setText("");
		 }
	}
	
	public boolean isAWord(String input){
		String firstChar = input.substring(0, 1);
		int result = 1;
		try{
            BufferedReader buff = new BufferedReader(new InputStreamReader(getAssets().open("words." + firstChar + ".txt")));
            String dictRead = "";
            	while ((dictRead = buff.readLine()) != null){
				   result = dictRead.compareToIgnoreCase(input);
				   if (result == 0){
					   buff.close();
					   return true;
				   }
				   if (result >  0){
					   buff.close();
					   return false;
				   }
				}
			buff.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void clearList(View view){
		letternum = 3;
		final TextView isWord = (TextView) findViewById(R.id.dabble_Words);
		EditText editText = (EditText) findViewById(R.id.edit_message_dabble);
		Button confirm = (Button) findViewById(R.id.button2);
		found = "";
		word3 = "";
		word4 = "";
		word5 = "";
		isWord.setText(found);
		editText.setText(found);
		editText.setHint("Enter a 3 letter word");
		confirm.setText("");
	}
	
	public void confirmButton(View view){
		if (letternum == 7){
			Intent intent = new Intent(this, Dabble_Multi_Activity.class);
			if (challenge){
				intent.putExtra("WordFlag", 3);
			} else {
			intent.putExtra("WordFlag", 1);
			}
			intent.putExtra("ID", ID);
			intent.putExtra("word3", word3);
			intent.putExtra("word4", word4);
			intent.putExtra("word5", word5);
			intent.putExtra("word6", word6);
			finish();
			startActivity(intent);
		}
	}
	
	public void backButton(View view){
		Intent intent = new Intent(this, Dabble_Multi_Activity.class);
		intent.putExtra("WordFlag", 0);
		finish();
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.multiplayer__dabble__create, menu);
		return true;
	}

}
