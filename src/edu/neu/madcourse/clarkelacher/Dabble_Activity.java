package edu.neu.madcourse.clarkelacher;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class Dabble_Activity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dabble, menu);
		return true;
	}

	public void newGame(View view){
		Intent intent = new Intent(this, Dabble_Single_Game.class);
		finish();
		startActivity(intent);
	}
	
	public void newMulti(View view){
		Intent intent = new Intent(this, Dabble_Multi_Activity.class);
		intent.putExtra("WordFlag", 0);
		startActivity(intent);
	}
	
	public void exitButton(View view){
		finish();
	}
	
	public void acknButton(View view) {
		Intent intent = new Intent(this, Main_Acknowledgements.class);
		startActivity(intent);
	}
}
