package edu.neu.madcourse.clarkelacher;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class Dabble_Single_Victory extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble_victory);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.victory, menu);
		return true;
	}
	
	public void newGame(View view){
		Intent intent = new Intent(this, Dabble_Single_Game.class);
		finish();
		startActivity(intent);
	}
	
	public void exitButton(View view){
		finish();
	}
	
}
