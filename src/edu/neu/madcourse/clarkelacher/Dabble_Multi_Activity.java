package edu.neu.madcourse.clarkelacher;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import edu.neu.mhealth.api.KeyValueAPI;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class Dabble_Multi_Activity extends Activity {
int isCreate;
String word3;
String word4;
String word5;
String word6;
String ID;
Context context;
boolean challenge = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dabble_multi);
		context = this;
		EditText et = (EditText) findViewById(R.id.edit_message);
		isCreate = 0;
		Bundle extras = getIntent().getExtras();
		isCreate = extras.getInt("WordFlag");
		ID = "noID";
		
		if (isCreate == 2){
			challenge = true;
		}
		
		if (isCreate >= 1){
			ID = extras.getString("ID");
			if (isCreate == 1 || isCreate == 3){
			word3 = extras.getString("word3");
			word4 = extras.getString("word4");
			word5 = extras.getString("word5");
			word6 = extras.getString("word6");
			new AsynchSetKey().execute(ID, word3, word4, word5, word6);
			}
			et.setText(ID);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.multiplayer__dabble, menu);
		return true;
	}
	
	/*
	public void notifyButton(View view){
		EditText et = (EditText) findViewById(R.id.edit_message);
		ID = et.getText().toString();
		String nService = Context.NOTIFICATION_SERVICE;
		NotificationManager nManager = (NotificationManager) getSystemService(nService);
		new Dabble_Async_Notification(this, nManager).execute(ID);
		//new AsynchSetNotification().execute(ID);
	}
	*/
	
	public void exitButton(View view){
		finish();
	}
	
	public void joinButton(View view){
		TextView tv = (TextView) findViewById(R.id.MultiDabble);
		EditText et = (EditText) findViewById(R.id.edit_message);
		tv.setText("Joining...");
		new AsynchJoinGame().execute(et.getText().toString());
	}
	
	public void createButton(View view){
		TextView tv = (TextView) findViewById(R.id.MultiDabble);
		EditText et = (EditText) findViewById(R.id.edit_message);
		if (et.getText().toString().length() > 2){
		tv.setText("Creating Key: " + et.getText().toString() + "...");
		new AsynchCreate().execute(et.getText().toString());
		}
		}
	
	public void create(String ID){
		Intent intent = new Intent(this, Dabble_Multi_Create.class);
		intent.putExtra("Challenge", challenge);
		intent.putExtra("ID", ID);
		finish();
		startActivity(intent);
	}
	
	public void checkButton(View view){
		EditText et = (EditText) findViewById(R.id.edit_message);
		TextView tv = (TextView) findViewById(R.id.MultiDabble);
		tv.setText("Getting Info...");
		new AsynchGetInfo().execute(et.getText().toString());
	}
	
	public void updateText(String newText){
		TextView tv = (TextView) findViewById(R.id.MultiDabble);
		tv.setText(newText);
	}
	
	private class AsynchJoinGame extends AsyncTask<String, Void, String> {
	    protected String doInBackground(String... params) {	    	
	    	String ID = params[0];
	    	String aword3 = "";
	    	String aword4 = "";
	    	String aword5 = "";
	    	String aword6 = "";
	    	long timeleft = 0;
	    	//int turns = 0;
	    	boolean newGame = false;
	    	boolean gameExists = false;
	    	int attempts = 0;
	    	
	    	System.out.println("Checking Internet Connection...");
    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
				attempts++;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
				if (KeyValueAPI.isServerAvailable()){
				String check1 = KeyValueAPI.get("chlacher", "qwer1234", ID);
				gameExists = (!check1.equalsIgnoreCase("Error: No Such Key"));
				System.out.println("Check1:" + check1 + ", so " + gameExists);
				
				if (gameExists){
				String check2 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword3");
				newGame = (check2.equalsIgnoreCase("Error: No Such Key"));
				
				if (newGame){
					aword3 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word3");
					System.out.println("word3 -" + aword3);
					aword4 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word4");
					System.out.println("word4 -" + aword4);
					aword5 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word5");
					System.out.println("word5 -" + aword5);
					aword6 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-word6");
					System.out.println("word6 -" + aword6);	
				} else{
					aword3 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword3");
					System.out.println("word3 -" + aword3);
					aword4 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword4");
					System.out.println("word4 -" + aword4);
					aword5 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword5");
					System.out.println("word5 -" + aword5);
					aword6 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword6");
					System.out.println("word6 -" + aword6);
					timeleft = Long.valueOf(KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedtime"));
					//turns = Integer.valueOf(KeyValueAPI.get("chlacher", "qwer1234", ID + "-turn"));		
					}
				
			
			if (newGame){
				return ID + "-" + aword3 + "-" + aword4 + "-" + aword5 + "-" + aword6 + "-";
			} else {
				return "RESUME" + "-" + ID + "-" + aword3 + "-" + aword4 + "-" + aword5 + "-" + aword6 + "-" + timeleft;
			}
	    }
				else return null;
		}
			else return "sunv";
				
	    } else return "sunv2";
    		
	    }
	    

	    @Override
	    protected void onPostExecute(String result) {
	    	TextView tv = (TextView) findViewById(R.id.MultiDabble);
	    	if (result == null){
	    		tv.setText("No Such Game. Try Another Key");
	    	} else if (result.equals("sunv")){
	    		tv.setText("Server Unavailable or network too slow. Please test Your Network and Try Again");
	    	} else if (result.equals("sunv2")){
	    		tv.setText("Internet Unavailable. If possible, switch to wifi");
	    	} else {
	    	context = getApplicationContext();
	    	System.out.println(result);
	    	String[] parts = result.split("-");
	    	
	    	boolean resume;
	     	String ID;
	    	String aword3;
	    	String aword4;
	    	String aword5;
	    	String aword6;
	    	long timeleft;
	    	//int turn;
	    	
	    	if (parts[0].equals("RESUME")){
	    	System.out.println("Resuming");
	    	resume = true;
	    	ID = parts[1];
	    	aword3 = parts[2];
	    	aword4 = parts[3];
	    	aword5 = parts[4];
	    	aword6 = parts[5];
	    	timeleft = Long.valueOf(parts[6]);
	    	//turn = Integer.valueOf(parts[7]);
	    	System.out.println("Time left:" + timeleft);
	    	
	    	} else{
	    	resume = false;
	    	ID = parts[0];
	    	aword3 = parts[1];
	    	aword4 = parts[2];
	    	aword5 = parts[3];
	    	aword6 = parts[4];
	    	timeleft = -1;
	    	//turn = -1;
	    	
	    	}
	    	
	    	Intent intent = null;
			if (resume){
				intent = new Intent(context,  Dabble_Multi_Game_Resume.class);
			}
			else{
				intent = new Intent(context,  Dabble_Multi_Game.class);
			}
			
			intent.removeExtra("gameID");
			intent.putExtra("gameID", ID);
			System.out.println(ID);
			intent.putExtra(ID + "word3", aword3);
			intent.putExtra(ID + "word4", aword4);
			intent.putExtra(ID + "word5", aword5);
			intent.putExtra(ID + "word6", aword6);
				if (resume){
				intent.putExtra(ID + "time", "" + timeleft);
				//intent.putExtra(ID + "turn", "" + turn);
				System.out.println("Put Extra -" + aword3 + aword4 + aword5 + aword6 + timeleft);
				}
				else{
				System.out.println("Put Extra -" + aword3 + aword4 + aword5 + aword6);
				}
				tv.setText("Welcome back. Please enter your new ID");
				startActivity(intent);
	    	}
	    	
	    }
	    

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}
	
	private class AsynchCreate extends AsyncTask<String, Void, String> {
	    
	    protected String doInBackground(String... params) {
	    	String input = params[0];
	    	System.out.println("Create - Checkpoint 1");
	    	int attempts = 0;
	    	
	    	System.out.println("Checking Internet Connection...");
    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
				attempts++;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
	    	
			if(KeyValueAPI.isServerAvailable()){
				System.out.println("Create - Checkpoint 2");
				if (KeyValueAPI.get("chlacher", "qwer1234", input).equalsIgnoreCase("Error: No Such Key")){
					System.out.println("Create - Checkpoint 3");
		    		return "key-" + input;
				}
				else{
					return "break-Key already Exists. Select Join, Check, or Enter a new Key";
				}
				
	    	}
	    	else return "break-Server Unavailable or connection too slow. Please test Your Network and Try Again";
	    } else return "break-Internet Unavailable. If possible, switch to wifi"; 
    		
	    }

	    @Override
	    protected void onPostExecute(String result) {
	    	String[] parts = result.split("-");
	    	String newText = "";
	    	String val = parts[1];
	    	
	    	if (parts[0].equals("break")){
	    		System.out.println("break");
	    		newText = val;
	    	} else{
	    		newText = "Creating Key: " + val;
	    		create(val);
	    	}
	    	updateText(newText);
	    	
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}
	
	private class AsynchSetKey extends AsyncTask<String, Void, String> {
		/*
		public void create(CallBackListener listener, String input){
			System.out.println("Create - Checkpoint 1");
			if(KeyValueAPI.isServerAvailable()){
				System.out.println("Create - Checkpoint 2");
				if (KeyValueAPI.get("chlacher", "qwer1234", input).equalsIgnoreCase("Error: No Such Key")){
					System.out.println("Create - Checkpoint 3");
		    		listener.callback(1, input);
				}
				else{
					listener.callback(0, "Key already Exists. Select Join, Check, or Enter a new Key");
				}
				
	    	}
	    	else listener.callback(0, "Server Unavailable. Please Check Your Network and Try Again");
	    }
	    */
	    
	    protected String doInBackground(String... params) {	    	
	    	int attempts = 0;
	    	// Attempts to ping the server every 6 seconds for 10 minutes
			while(attempts < 100 && !(KeyValueAPI.isServerAvailable())){
				attempts++;
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String input = params[0];
			word3 = params[1];
			word4 = params[2];
			word5 = params[3];
			word6 = params[4];
			
			String gameNum = (KeyValueAPI.get("chlacher", "qwer1234", input + "-gameNum"));
			
			if (gameNum.equalsIgnoreCase("Error: No Such Key")){
				gameNum = "0";
			} else {
				gameNum = "" + (Integer.valueOf(gameNum) + 1);
			}
			
			if (KeyValueAPI.isServerAvailable()){
			String val = input + "-GameID";
			String stats = input + "-Stats";
			String gameword3 = input + "-word3";
			String gameword4 = input + "-word4";
			String gameword5 = input + "-word5";
			String gameword6 = input + "-word6";
			String gameStatus = input + "-gameStatus";
			KeyValueAPI.put("chlacher", "qwer1234", input, val);
			KeyValueAPI.put("chlacher", "qwer1234", gameword3, word3);
			KeyValueAPI.put("chlacher", "qwer1234", gameword4, word4);
			KeyValueAPI.put("chlacher", "qwer1234", gameword5, word5);
			KeyValueAPI.put("chlacher", "qwer1234", gameword6, word6);
			KeyValueAPI.put("chlacher", "qwer1234", input + "-gameNum", gameNum);
			if (isCreate == 3){
				KeyValueAPI.put("chlacher", "qwer1234", gameStatus, "Challenge");
	    	}
			if (isCreate == 1){
				KeyValueAPI.put("chlacher", "qwer1234", gameStatus, "New");
	    	}
			KeyValueAPI.put("chlacher", "qwer1234", stats, input + ": Stats go here");
			} else{
				System.out.println("Gave up on adding");
			}
			
			return gameNum;
	    }        

	    @Override
	    protected void onPostExecute(String result) {
	    	String nService = Context.NOTIFICATION_SERVICE;
			NotificationManager nManager = (NotificationManager) getSystemService(nService);
			new Dabble_Async_Notification(getApplicationContext(), nManager).execute(ID, result);
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}
/*
	@Override
	public void callback(int n, String val) {
		System.out.println("Step five");
		if (n == 0){
			updateText(val);
		}
		if (n == 1){
			create(val);
		}
		
	}
*/
	
	private class AsynchGetInfo extends AsyncTask<String, Void, String> {
	    
	    	protected String doInBackground(String... params) {	    	
	    		String ID = params[0];
	    		String aword3 = "Not Yet Populated";
	    		String aword4 = "Not Yet Populated";
	    		String aword5 = "Not Yet Populated";
	    		String aword6 = "Not Yet Populated";
	    		String score = "0";
	    		String time = "65";
	    		String status = "";
	    		int attempts = 0;		    		
	    		
	    		
	    		System.out.println("Checking Internet Connection...");
	    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

	    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
					attempts++;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
	    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
	    			System.out.println("Pinging Server...");
				if (KeyValueAPI.isServerAvailable()){
					System.out.println("Success");	
					String check = KeyValueAPI.get("chlacher", "qwer1234", ID);
					System.out.println(check);
					if (!check.equals("Error: No Such Key")){	
				
						status = KeyValueAPI.get("chlacher", "qwer1234", ID + "-gameStatus");	
				
						if (!status.equals("New")){
							aword3 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword3");
							System.out.println("word3 -" + aword3);
							aword4 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword4");
							System.out.println("word4 -" + aword4);
							aword5 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword5");
							System.out.println("word5 -" + aword5);
							aword6 = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedword6");
							System.out.println("word6 -" + aword6);
							time = KeyValueAPI.get("chlacher", "qwer1234", ID + "-storedtime");
							score = KeyValueAPI.get("chlacher", "qwer1234",	ID + "-storedscore");
						}
						
						
				} else {
					return "break-Invalid Key";
				}
			
			}
				else{
					return "break-Server Unavailable or internet too slow. Please test your network and try again";
				}
			
			return ID + "-" + aword3 + "-" + aword4 + "-" + aword5 + "-" + aword6  + "-" + score  + "-" + time  + "-" + status;
			
		    }	else return "break-Internet Unavailable. Switch to Wifi if Possible";
	    }

	    @Override
	    protected void onPostExecute(String result) {
	    	context = getApplicationContext();
	    	System.out.println(result);
	    	String[] parts = result.split("-");
	    	String newText = "";
	    	
	    	if (parts[0].equals("break")){
	    		System.out.println("break");
	    		newText = parts[1];
	    	} else{
	    	String ID = parts[0];
	    	String aword3 = parts[1];
	    	String aword4 = parts[2];
	    	String aword5 = parts[3];
	    	String aword6 = parts[4];
	    	String score = parts[5];
	    	String time = parts[6];
	    	String status = parts[7];
	    	
	    	int displaytime = (int) (Long.valueOf(time)/1000);
	    	newText = "id: " + ID + "\nword 1: " + aword3 + "\nword 2: " + aword4 + "\nword 3: " + aword5 + "\nword 4: " + aword6 + "\nScore: " + score + "\nStatus: " + status + "\nTime Left: " + displaytime;
	    	}
	    	TextView tv = (TextView) findViewById(R.id.MultiDabble);
	    	tv.setText(newText);
	    	
	    }

	    @Override
	    protected void onPreExecute() {
	    }

	    @Override
	    protected void onProgressUpdate(Void... values) {
	    }
	}

}




