package edu.neu.madcourse.patrickcincotta;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import edu.neu.mobileClass.PhoneCheckAPI;

public class MainMenuActivity extends Activity {

	public final static String EXTRA_MESSAGE = "edu.neu.madcourse.patrickcincotta.MESSAGE";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	PhoneCheckAPI.doAuthorization(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);
        
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId().toString();
		DabblePolling poll = new DabblePolling(this);
		
		poll.doInBackground("poll", imei);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void test2PlayerDabble(View v){
        Intent intent = new Intent(this, DabbleScoreScreen.class);
        intent.putExtra("userScore", "0");
    	startActivity(intent);
    }
    
    public void commHunt(View v){
        Intent intent = new Intent(this, CommHuntStartActivity.class);
    	startActivity(intent);
    }
 
    public void test2PlayerDabblePt2(View v){
        Intent intent = new Intent(this, DabbleChallenge.class);
        intent.putExtra("userName", "");
    	startActivity(intent);
    }
    
    
    public void startSudokuButtonClicked(View view) {
    	//launch Sudoku
    	Intent intent = new Intent(this, Sudoku.class);
    	startActivity(intent);
    }
    
    public void quitButtonClicked(View view) {
    	//quit the app
    	finish();
    	
    }
    
    public void startDictionaryButtonClicked(View view) {
    	//launch the dictionary app
    	Intent intent = new Intent(this, Dictionary.class);
    	startActivity(intent);
    }
    
    public void errorButtonClicked(View view) {
    	//generates the error as specified
    	Intent intent = new Intent(this, MainMenuGenError.class);
    	startActivity(intent);
    	
    }
    
    public void dabButtonClicked(View view) {
    	//generates the error as specified
    	Intent intent = new Intent(this, DabbleActivity.class);
    	startActivity(intent);
    	
    }
    public void description(View view) {
		PackageManager pm = getPackageManager();
		Intent intent = pm.getLaunchIntentForPackage("edu.neu.madcourse.clarkelacher");
		startActivity(intent);	
    
    }

    
    public void aboutButtonClicked(View view) {
    	//show the about page
    	Intent intent = new Intent(this, MainMenuAboutActivity.class);
    	
    	TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
    	String imei = telephonyManager.getDeviceId();
    	
    	intent.putExtra(EXTRA_MESSAGE, imei);
    	startActivity(intent);
    }
    
}
