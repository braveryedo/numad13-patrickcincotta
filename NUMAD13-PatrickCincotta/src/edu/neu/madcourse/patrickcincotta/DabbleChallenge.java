package edu.neu.madcourse.patrickcincotta;

import java.util.Calendar;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import edu.neu.mhealth.api.KeyValueAPI;
import edu.neu.madcourse.patrickcincotta.DabblePolling;

public class DabbleChallenge extends Activity {

	String user;//username passed from previous screen
	EditText uf; //user (challenger)
	EditText cf;// challenged user
	TextView ed;// error display
	TextView op; // online players
	String onlineFormatted;
	Handler h;
	String errorText;
	String[] times;
	String[] userArr;
	boolean[] online;
	boolean d;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dabble_challenge);
		Intent i = getIntent();
		user = i.getStringExtra("userName");
		Log.d("Dabble challenge", "got " + user + " from previous activity");
		uf = (EditText) findViewById(R.id.dab_challenger);
		cf = (EditText) findViewById(R.id.dab_challenged);
		ed = (TextView) findViewById(R.id.dab_errorDisplay);
		op = (TextView) findViewById(R.id.dab_onlinePlayers);
		uf.setText(user);
		h = new Handler();
		updOnline(null);
		d = true;
	}
	
	public void regName (View v){
		//register a username to the phone'imei
		
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId().toString();
		String username = uf.getText().toString();
		DabblePolling poll = new DabblePolling(this);
		poll.doInBackground("poll", imei);
		
		Calendar c = Calendar.getInstance();
		long secondsSinceEpoch = c.getTimeInMillis()/1000;
		long mins = secondsSinceEpoch/60;
		
		String timeList = new DabbleAsynch().doInBackground("get", "TimeStamps");
		String userList = new DabbleAsynch().doInBackground("get", "UserList");
		
		if(username.length() < 2){
			setError("username too short");
		} else {
			if(userList.equals("Error: No Such Key")|| userList.equals("")){
				new DabbleAsynch().doInBackground("put", "UserList", username);
				new DabbleAsynch().doInBackground("put","TimeStamps", ""+mins);
				Log.d("Dabble challenge screen", "no other users found, this is the first user");
			}else if(userList.contains("server unavailable")){
				Log.d("Dabble challenge screen", "server unavailable");
				setError("server unavailable");
			} else if(!userList.contains(username)){
				
				new DabbleAsynch().doInBackground("put", "UserList", userList + "," + username);
				new DabbleAsynch().doInBackground("put","TimeStamps", timeList + "," + mins);
				Log.d("Dabble challenge screen", "added: " + username +  " to userList: " + userList);
				
			} else {
				Log.d("Dabble challenge screen", "user already added, updating timestamp");
				String[] ulArr = userList.split(",");
				int index = 0;
				
				for (int i = 0; i < ulArr.length; i++){
					if (username.equals(ulArr[i])){
						index = i;
					}
				}
				
				String[] tsArr = timeList.split(",");
				tsArr[index] ="" + mins;
				
				timeList = "";
				for (int i = 0; i < tsArr.length; i++){
					timeList += tsArr[i]+",";
				}
				
				timeList = timeList.substring(0,timeList.length()-1);
				
				new DabbleAsynch().doInBackground("put","TimeStamps", timeList);
				
			}	
		}
		
		
		new DabbleAsynch().doInBackground("put", username, imei);
		setError("Username: " + uf.getText().toString() + " registered to imei: " + imei);
		
		updOnline(null);
	}
	
	public void challenge(View v){
		
		if(KeyValueAPI.isServerAvailable()){	
			String chal = uf.getText().toString();//challenger
			String chald = cf.getText().toString();//challenged
			boolean userExists = false;
			
			for (int i = 0; i < userArr.length; i++){
				if (userArr[i].equals(chald)){
					userExists = true;
				}
			}
			
			if (userExists){
				DabblePolling poll = new DabblePolling(this);
				poll.doInBackground("challenge", chal, chald);
				setError("Sending challenge...");
				
				
				Intent i = new Intent(this, DabbleActivity.class);
				
				
				
			} else {
				setError("The user you are trying to challenge does not exist");
			}
		} else {
			setError("server unavilable, please try again later");
		}
		
	}
	
	
	public void challengeQuit(View v){
		finish(); 
	}	
	public void setError(String e){
		errorText = e;
		h.post(errorSetter);
	}
	
	private Runnable errorSetter = new Runnable() {
		   @Override
		   public void run() {
			   
				if (ed.getText().toString().length() < 1){
					ed.setText(errorText);
					h.postDelayed(this, 2000);
				} else {
					ed.setText("");
					errorText = "";
				}
		   }
		};
		
	public void updOnline(View v){
		
		Calendar c = Calendar.getInstance();
		long secondsSinceEpoch = c.getTimeInMillis()/1000;
		long mins = secondsSinceEpoch/60;
		onlineUsers(mins);
		d = true;
	}
	
	public void onlineUsers(long time){
		String timeString = new DabbleAsynch().doInBackground("get", "TimeStamps");
		String users = new DabbleAsynch().doInBackground("get", "UserList");
		
		if(timeString.equals("Error: No Such Key")){
			setError("No users have been added to the system yet");
		} else if(timeString.equals("server unavailable")){
			setError(timeString);
		} else {
		
			times = timeString.split(",");
			userArr = users.split(",");
			online = new boolean[userArr.length];
		
			for (int i = 0; i < times.length; i++){
				//users are considered online if they have updated their time stamp within the last 2 hours
				online[i] = (time - Long.parseLong(times[i]) < 120);
				Log.d("Dabble challenges", "user " + i + " time: "+ times[i]);
			}
			displayOnline(true);
		}
		
	}
	
	public void togglePlayersDisplayed(View v){
		if(d){
			displayOnline(false);
			d = false;
		} else {
			updOnline(null);
			d = true;
		}
		
	}
	
	//if b is true, display all online users
	//if b is false display all users
	public void displayOnline(boolean b){
		String temp;
		if(b){
			temp = "Online Users\n";
		} else {
			temp = "All Users\n";
		}
		
		for (int i = 0; i < online.length; i++){
			if(!b || online[i]){
				temp += userArr[i] + "\n";
			}
		}
		onlineFormatted = temp;
		op.setText(onlineFormatted);
	}
		
	private class DabbleAsynch extends AsyncTask<String, Void, String>{
		
		protected String doInBackground(String...keys) {
			
			if(!KeyValueAPI.isServerAvailable()){
				return "server unavailable"; //server not available code
			}
			String returns = "";
			
			if(keys[0].compareTo("get")==0){
					String[] stuff = keys[1].split(",");
					
					for (int i = 0; i < stuff.length; i++){
						Log.d("Dabble_score_screen: ","getting: "+stuff[i]);
						returns += KeyValueAPI.get("patcinc", "x2x4x6x8", stuff[i]);
						
					}
			} else if(keys[0].equals("put")) {
				Log.d("Dabble_score_screen: ","putting: " + keys[2]);
				returns = KeyValueAPI.put("patcinc", "x2x4x6x8", keys[1], keys[2]);
			} else {
				Log.d("Dabble_score_screen: ",keys[0] + " is not a valid command, look at your code T_T");
			}
			return returns;
		}
	}

}
