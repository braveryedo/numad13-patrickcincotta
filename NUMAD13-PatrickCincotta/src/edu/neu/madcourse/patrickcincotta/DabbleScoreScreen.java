package edu.neu.madcourse.patrickcincotta;

import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import edu.neu.mhealth.api.KeyValueAPI;

public class DabbleScoreScreen extends Activity {
	String userScore;
	String user = "";
	String HS;
	String HSU;
	Handler h;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dabble_score_screen);
		getHighScoringUsers();
		getScores();
		displayScores();
		Intent i = getIntent();
		userScore = i.getStringExtra("userScore");
		TextView sf = (TextView) findViewById(R.id.dab_scoreField);
		sf.setText(userScore);
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dabble_score_screen, menu);
		return true;
	}
	
	public void clearAll(View v){
		new DabbleAsynch().doInBackground("put", "HighScoringUsers", "Blank0,Blank1,Blank2,Blank3,Blank4,Blank5,Blank6,Blank7,Blank8,Blank9");
		new DabbleAsynch().doInBackground("put", "HighScores", "0,1,2,3,4,5,6,7,8,9");
		new DabbleAsynch().doInBackground("put", "UserList", "");
		new DabbleAsynch().doInBackground("put", "TimeStamps", "");
		
		TextView uf = (TextView) findViewById(R.id.dab_userField);
		TextView sf = (TextView) findViewById(R.id.dab_scoreField);
		
		uf.setText("");
		sf.setText("");
		
		getScores();
		getHighScoringUsers();
		
		
		displayScores();
		
	}
	
	public void displayScores(){
		TextView us =  (TextView) findViewById(R.id.testScores);
		TextView hs =  (TextView) findViewById(R.id.testScores2);
		
		String[] tempUsers = HSU.split(",");
		String[] tempScores = HS.split(",");

		String formattedUsers = "# \t User \n";
		String formattedScores = "Scores \n";
		
		for (int i = 0; i < tempScores.length; i++){
			formattedUsers += i + 1 + " \t "+ tempUsers[i] + "\n";
			formattedScores += tempScores[i] + "\n";
		}
		
		us.setText(formattedUsers);
		hs.setText(formattedScores);
		Log.d("Dabble scores"," display scores end");
		/* Trying to get the format to look like this:
		 * 
		 *1	USER1		USERSCORE1
		 *2 USER2		USERSCORE2
		 *3	USER3		USERSCORE3
		 *...
		 *10 USER10		USERSCORE10 
		 * 
		 */
		
	}
	
	public void submitScore(View v){
		addUser();
		
		TextView uf = (TextView) findViewById(R.id.dab_userField);
		TextView sf = (TextView) findViewById(R.id.dab_scoreField);
		
		String score = sf.getText().toString();
		String userName = uf.getText().toString();
		
		getHighScoringUsers();
		getScores();
		
		String[] tempUsers = HSU.split(",");
		String[] tempScores = HS.split(",");
		
		String buildUsers = "";
		String buildScores = "";
		
		boolean userAddedToHS = false;
		int scoreInt = Integer.parseInt(score);
		
		for (int i = 0; i < tempScores.length; i++){
			
			if (Integer.parseInt(tempScores[i]) < scoreInt && !userAddedToHS){
				Log.d("Dabble score screen" , "submiting score replacing: " + i);
				buildUsers += userName;
				buildScores += score;
				userAddedToHS = true;
				
			} else if(userAddedToHS){
				Log.d("Dabble score screen" , "submiting score already replaced and at: " + i);
				buildUsers += tempUsers[i-1];
				buildScores += tempScores[i-1];
				
			} else {
				Log.d("Dabble score screen" , "submiting score not replaced and at: " + i);
				buildUsers += tempUsers[i];
				buildScores += tempScores[i];				
			}
			
			buildUsers += ",";
			buildScores += ",";
		}
		
		buildUsers = buildUsers.substring(0,buildUsers.length()-1); //removes trailing comma
		buildScores = buildScores.substring(0,buildScores.length()-1);//removes trailing comma
		
		if(!userAddedToHS){
			Log.d("Dabble score screen" , "submit score failed, score too low");
			h = new Handler();
			h.post(lowScore);
		} else {
			
			new DabbleAsynch().doInBackground("put", "HighScores", buildScores);
			new DabbleAsynch().doInBackground("put", "HighScoringUsers", buildUsers);
			
			HS = buildScores;
			HSU = buildUsers;
			
			Log.d("Dabble score screen" , "score submitted!");
		}
		
		displayScores();
		
	}
	
	
	
	public void getScores(){
		HS = new DabbleAsynch().doInBackground("get", "HighScores");
		if (HS.equals("Error: No Such Key")){
			Log.d("Dabble score screen" , "didnt find highscores");
			//new DabbleAsynch().doInBackground("set", "HighScores", "0,0,0,0,0,0,0,0,0,0");
			getScores();
		} else if(HS.equals("server unavailable")){
			Log.d("Dabble score screen" , "highscores: server unavilable");
			//HS = "0,0,0,0,0,0,0,0,0,0";
		} else {
			Log.d("Dabble score screen" , "highscores retrieved!");
		}
	}
	
	public void getHighScoringUsers(){
		HSU = new DabbleAsynch().doInBackground("get", "HighScoringUsers");
		if(HSU.equals("Error: No Such Key")){
			Log.d("Dabble score screen" , "didnt find highscores users");
			//new DabbleAsynch().doInBackground("set", "HighScoringUsers", "Blank,Blank,Blank,Blank,Blank,Blank,Blank,Blank,Blank,Blank");
			getHighScoringUsers();
		} else if(HSU.equals("server unavailable")){
			Log.d("Dabble score screen" , "highscoringusers: server unavilable");
			//HSU = "Blank,Blank,Blank,Blank,Blank,Blank,Blank,Blank,Blank,Blank";
		} else {
			Log.d("Dabble score screen" , "highscoringusers retreived!");
		}
	}
	private Runnable lowScore = new Runnable() {
		   @Override
		   public void run() {
			   
				Button submit = (Button) findViewById(R.id.dab_submitScore);
				if (submit.getText().toString().equals("Submit Score")){
					submit.setText("Score Too Low");
					h.postDelayed(this, 2000);
				} else {
					submit.setText("Submit Score");
				}
		   }
		};
	
	
	public void addUser(){
		Calendar c = Calendar.getInstance();
		long secondsSinceEpoch = c.getTimeInMillis()/1000;
		long mins = secondsSinceEpoch/60;
		
		TextView uf = (TextView) findViewById(R.id.dab_userField);
		user = uf.getText().toString();
		String timeList = new DabbleAsynch().doInBackground("get", "TimeStamps");
		String userList = new DabbleAsynch().doInBackground("get", "UserList");
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId().toString();
		
		if(user.length() < 2){
			Log.d("Dabble score screen" , "username too short");
		} else {
			if(userList.compareTo("Error: No Such Key") == 0 || userList.equals("")){
				new DabbleAsynch().doInBackground("put", "UserList", user);
				new DabbleAsynch().doInBackground("put","TimeStamps", ""+mins);
				new DabbleAsynch().doInBackground("put", user , imei);
				Log.d("Dabble score screen", "no other users found, this is the first user");
			}else if(userList.contains("server unavailable")){
				Log.d("Dabble score screen", "server unavailable");
			}else if(userList.contains("Error: No Such Key")){
				Log.d("Dabble score screen", userList);
			} else if(!userList.contains(user)){
				
				new DabbleAsynch().doInBackground("put", "UserList", userList + "," + user);
				new DabbleAsynch().doInBackground("put","TimeStamps", timeList + "," + mins);
				new DabbleAsynch().doInBackground("put", user , imei);
				Log.d("Dabble score screen", "added: " + user +  " to userList: " + userList);
				
			} else {
				Log.d("Dabble score screen", "user already added, updating timestamp");
				String[] ulArr = userList.split(",");
				int index = 0;
				
				for (int i = 0; i < ulArr.length; i++){
					if (user.equals(ulArr[i])){
						index = i;
					}
				}
				
				String[] tsArr = timeList.split(",");
				tsArr[index] ="" + mins;
				
				timeList = "";
				for (int i = 0; i < tsArr.length; i++){
					timeList += tsArr[i]+",";
				}
				
				timeList = timeList.substring(0,timeList.length()-1);
				
				new DabbleAsynch().doInBackground("put","TimeStamps", timeList);
				new DabbleAsynch().doInBackground("put", user , imei);
				
			}	
		}
	}
	
	public void userTest(View v){ //to do
		
		String userList = new DabbleAsynch().doInBackground("get", "UserList");
		
		if(userList.contains("Error: No Such Key") || userList.contains("server unavailable")){
			Log.d("Dabble score screen", userList);
		} else {
			Log.d("Dabble score screen", "here are the list of users: " + userList);
		}
	
	}
	
	public void quit(View v){
		
		finish();
	}
	
	public void more(View v){
		Intent intent = new Intent(this, DabbleChallenge.class);
		TextView uf = (TextView) findViewById(R.id.dab_userField);
        intent.putExtra("userName", uf.getText().toString());
    	startActivity(intent);
	}
	
	
	private class DabbleAsynch extends AsyncTask<String, Void, String>{
		
		protected String doInBackground(String...keys) {
			
			if(!KeyValueAPI.isServerAvailable()){
				return "server unavailable"; //server not available code
			}
			String returns = "";
			
			if(keys[0].compareTo("get")==0){
					String[] stuff = keys[1].split(",");
					
					for (int i = 0; i < stuff.length; i++){
						Log.d("Dabble_score_screen: ","getting: "+stuff[i]);
						returns += KeyValueAPI.get("patcinc", "x2x4x6x8", stuff[i]);
						
					}
			} else if(keys[0].equals("put")) {
				Log.d("Dabble_score_screen: ","putting: " + keys[2]);
				returns = KeyValueAPI.put("patcinc", "x2x4x6x8", keys[1], keys[2]);
			} else {
				Log.d("Dabble_score_screen: ",keys[0] + " is not a valid command, look at your code T_T");
			}
			return returns;
		}
	}
}
