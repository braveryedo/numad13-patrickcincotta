package edu.neu.madcourse.patrickcincotta;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class MainMenuGenError extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        for (int i = 0; i>=0; i++){
        		i=0; //will time out due to infinite loop on creation
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.about, menu);
        return true;
    }
    
}