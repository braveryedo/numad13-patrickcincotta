package edu.neu.madcourse.patrickcincotta;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class DabbleHintActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dabble_hint);
		
		ArrayList<String> ar = DabbleActivity.cheaterCheaterPumpkinEater;
		
		TextView threeword = (TextView) findViewById(R.id.threeWordHint);
		TextView fourword = (TextView) findViewById(R.id.fourWordHint);
		TextView fiveword = (TextView) findViewById(R.id.fiveWordHint);
		TextView sixword = (TextView) findViewById(R.id.sixWordHint);
		
		threeword.setText("Three: " + ar.get(1) + ar.get(2) + ar.get(3));
		fourword.setText("Four: " + ar.get(4) + ar.get(5) + ar.get(6) + ar.get(7));
		fiveword.setText("Five: " + ar.get(8) + ar.get(9) + ar.get(10) + ar.get(11) + ar.get(12));
		sixword.setText("Six: " + ar.get(13) + ar.get(14) + ar.get(15) + ar.get(16) + ar.get(17) + ar.get(18));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hint, menu);
		return true;
	}
	
	public void leaveHint(View v){
		finish();
	}

}
