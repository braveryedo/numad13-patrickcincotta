package edu.neu.madcourse.patrickcincotta;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class Dictionary extends Activity{

	EditText et;
	TextView wf;
	String words; 
	ArrayList<String> displayedWords= new ArrayList<String>();

	InputStream istream;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dictionary);   
		
		words = "";
		final EditText et = (EditText) findViewById(R.id.dictionaryEntryField);
		final TextView wf = (TextView) findViewById(R.id.wordListField);

		displayedWords = new ArrayList<String>();
		

		//see source 1 in acknowledgments
		et.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				boolean isWord = false;

				Log.d("Dictionary activity, Text entered: ", s.toString());
				
				if(s.toString().length() >= 3){
					Log.d("Dictionary activity, word checking: ", "about to check word");
					isWord = checkWord(s);
					
					if (isWord){
						Log.d("Dictionary activity, word checking: ", "word found");
						wf.setText(words);
					} else {
						Log.d("Dictionary activity, word checking: ", "word NOT found");	
					}
				}

			}

		});

	}

	private boolean checkWord(CharSequence s) {
		String q = s.toString();
		String firstChar = q.substring(0,1);
		Boolean result = false;
		String fileName = firstChar + "words.txt";

			Log.d("Dictionary activity, checkWord func: ", "about to open istream");
			
			try{
			istream = this.getAssets().open(fileName);
			Log.d("Dictionary activity, checkWord func: ", "istream open");
			BufferedReader buff = new BufferedReader(new InputStreamReader(istream));
			String line = "";
			line = buff.readLine();
			
			
			while (line != null){
				int comp = line.compareToIgnoreCase(q);
				
				Log.d("Dictionary Activity, checkword: ", "comparing " + q + " to " + line);
				
				if(comp == 0){
					result = true;
					Log.d("Dictionary activity, checkword: ", "word found" + comp);
					buff.close();
					istream.close();
					addWord(q);
					break;
				} else if(comp > 0){
					Log.d("Dictionary activity, checkword: ", "word not found" + comp);
					buff.close();
					istream.close();
					break;
				}
				line = buff.readLine();
				
			}
			
			
			} catch (IOException e) {
				Log.d("Dictionary getting context: ", e.toString());
			}
		
		return result;
	}

	private void addWord(String word){
		if (!displayedWords.contains(word)){
			
			displayedWords.add(word);
			
			if (words.compareToIgnoreCase("") == 0){
				words = word;
			} else {
				String nextWord = ", " + word;
				words += nextWord;
			}
		}
	}

		@Override
		protected void onResume() {
			super.onResume();

		}

		@Override
		protected void onPause() {
			super.onPause();

		}


		public void ackButtonClicked(View view) {

			Intent intent = new Intent(this, DictionaryAcknowledgements.class);
			startActivity(intent);

		}

		public void dictionaryReturnButtonClicked(View view) {


			finish();

		}

		public void dictionaryClearButtonClicked(View view) {

			words = "";
			displayedWords.clear();
			TextView clearingWords = (TextView) findViewById(R.id.wordListField);
			clearingWords.setText("");
			EditText clearingEntryField = (EditText) findViewById(R.id.dictionaryEntryField);
			clearingEntryField.setText("");

		}



	}