package edu.neu.madcourse.patrickcincotta;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class DictionaryAcknowledgements extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dictionary_acknowledgements);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.acknowledgements, menu);
		return true;
	}

}
