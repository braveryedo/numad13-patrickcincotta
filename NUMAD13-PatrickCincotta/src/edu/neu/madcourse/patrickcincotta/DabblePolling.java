package edu.neu.madcourse.patrickcincotta;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import edu.neu.mhealth.api.KeyValueAPI;

public class DabblePolling extends   AsyncTask<String, Void, String>{
	Context context;
	Handler h = new Handler();
	String imei;
	String seeds = "none";
	String ch1, ch2, watch,scores,nums,words;
	CharSequence toastMessage;
	Builder alert;
	boolean disp = false;
	
	public DabblePolling (Context c){
		context = c;
	}
	
	
	
	protected String doInBackground(String...keys) {
		
		if(!KeyValueAPI.isServerAvailable()){
			return "server unavailable"; //server not available code
		}
		String returns = "";
		
		if(keys[0].compareTo("get")==0){
				String[] stuff = keys[1].split(",");
				
				for (int i = 0; i < stuff.length; i++){
					Log.d("Dabble polling: ","getting: "+stuff[i]);
					returns += KeyValueAPI.get("patcinc", "x2x4x6x8", stuff[i]);
					
				}
		} else if(keys[0].equals("put")||keys[0].equals("set")) {
			Log.d("Dabble polling: ","putting: " + keys[2]);
			returns = KeyValueAPI.put("patcinc", "x2x4x6x8", keys[1], keys[2]);
			
		} else if(keys[0].equals("challenge")){
			Log.d("Dabble polling:", "adding the challenge to the server");
			ch1 = keys[1];
			ch2 = keys[2];
			h.post(challenge);
			
		} else if(keys[0].equals("poll")){
			
			watch = keys[1];
			Log.d("Dabble polling:", "polling for notifications");
			h.post(pollForNotification);
			
		} else if(keys[0].equals("notify")){
			
			Log.d("Dabble polling:", "notifying opponent of change in score");
			ch1 = keys[1];
			ch2 = keys[2];
			scores = keys[3];
			nums = keys[4];
			words = keys[5];
			
			h.post(updateScore);
			
		}	else if(keys[0].equals("nextBoard")){
			
			Log.d("Dabble polling:", "setting seeds to the seeds of the next board");
			ch1 = keys[1];
			ch2 = keys[2];
			seeds = keys[3];
			h.post(updateSeed);
		}else if(keys[0].equals("removeNotification")){
			removeNotification();
		}	else { 
		
			
			Log.d("Dabble polling: ",keys[0] + " is not a valid command, look at your code T_T");
			returns = "invalid command";
		}
		
		return returns;
	}  
	
	
	public void postNotification(String challenger, String challenged){
		
		String ext = challenger;
		String ext2 = challenged;	

	    NotificationCompat.Builder notifyBuilder =  
	            new NotificationCompat.Builder(context)  
	            .setSmallIcon(R.drawable.ic_launcher)  
	            .setContentTitle("Dabble Challenge")  
	            .setContentText(ext + " has challenged: " + ext2);
	        
	    Intent notificationIntent = new Intent(context, DabbleActivity.class);
	    notificationIntent.putExtra("challenger", ext);
	    notificationIntent.putExtra("challenged", ext2);
	    PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);  
	    notifyBuilder.setContentIntent(contentIntent);  
	    NotificationManager notifier = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    
		if(KeyValueAPI.isServerAvailable()){
			
			String[] times,userArr;
			
			Calendar c = Calendar.getInstance();
			long secondsSinceEpoch = c.getTimeInMillis()/1000;
			long mins = secondsSinceEpoch/60;
			
			long time = mins;
			
			String timeString = doInBackground("get", "TimeStamps");
			String users = doInBackground("get", "UserList");
			
			if(!timeString.equals("Error: No Such Key")){
			
				times = timeString.split(",");
				userArr = users.split(",");
				int ind = 0;
				
				for (int i = 0; i < userArr.length; i++){
					if (userArr[i].equals(ext2)){
						ind = i;
					}
				}
				
				if (!disp){//time - Long.parseLong(times[ind]) < 120){
					
				    alert = new AlertDialog.Builder(context).setTitle("Challenge Recieved!")
				    		.setMessage(ext + " has challenged: " + ext2)
				    		.setCancelable(true)
				    		.setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
												    			public void onClick(DialogInterface dialog,int id){
												    				dialog.cancel();
												    			}
												    		}
				    							)
				    		.setPositiveButton("Accept",  new DialogInterface.OnClickListener() {
											    			public void onClick(DialogInterface dialog,int id){
											    				
											    				Intent intent = new Intent(((Dialog) dialog).getContext(), DabbleActivity.class);
											    				intent.putExtra("challenger", ch1);
											    			    intent.putExtra("challenged", ch2);
											    		    	context.startActivity(intent);
											    		    	
											    				dialog.cancel();
											    				
											    			}
											    		}
				    							)
				    		.setNeutralButton("Delete",  new DialogInterface.OnClickListener() {
				    			public void onClick(DialogInterface dialog,int id){
				    				doInBackground("put", imei, "0");
				    				dialog.cancel();
				    				
				    			}
				    		}
					);
				    
				    h.post(alerter);
				} else {

					notifier.notify("DabbleChallengeNotification" , 9001, notifyBuilder.build());
				}
							
				
				
			} else {

				notifier.notify("DabbleChallengeNotification" , 9001, notifyBuilder.build());
			}
		}
		

	      
	    
}


// Remove notification  
public void removeNotification() {  

if (imei == null){
  TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
  imei = telephonyManager.getDeviceId().toString();
}
Log.d("Dabble polling","removing notification for: " + imei);
doInBackground("put", imei, ""+0);
NotificationManager notifier = (NotificationManager)  context.getSystemService(Context.NOTIFICATION_SERVICE);
notifier.cancel("DabbleChallengeNotification" , 9001);
}


public void createGame(String challenger, String challenged, String s){

ch1 = challenger;
ch2 = challenged;

//challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)



String g = doInBackground("get", challenger + ":" + challenged);

if (g.equals("Error: No Such Key")||g.equals("server unavailable")){

		h.post(createG);
} else {

//challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)	
	String[] tempArr = g.split(",");
	String value = tempArr[0] + "," + tempArr[1] + "," + tempArr[2] + "," + ((Integer.parseInt(tempArr[3])+1)) + "," + s;
	doInBackground("put", ch1 + ":" + ch2, value);

}
}


	private Runnable challenge = new Runnable(){
		   @Override
		   public void run() {
			  Log.d("Dabble polling","adding challenge...");
			  String tempimei = doInBackground("get", ch2);
			  
			  if (!(tempimei.equals("server unavailable"))){
			  		doInBackground("put", tempimei, ch1+":"+ch2);
			  	//challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)
			  		createGame(ch1, ch2, "none");
			  		h.removeCallbacks(challenge);
			  } else {
				  Log.d("Dabble polling", "server unavilable while polling for: " + ch2);
				  	h.postDelayed(this, 15000);
			  }
	   }
	};
	
	
	Runnable createG = new Runnable(){
		   @Override
		   public void run() {
			   String temp = doInBackground("put", ch1+":"+ch2, ch1+":"+ch2+","+"0:0,0:0,1," + seeds);
			   
			   if(temp.equals("server unavailable")){
				   h.postDelayed(this, 2000);
			   } else {
				   h.removeCallbacks(this);
			   }
		   }
	};
	
	
	Runnable nextG = new Runnable(){
		   @Override
		   public void run() {
			   
			   String g = doInBackground("get", ch1 + ":" + ch2);
			   if(g.equals("server unavailable")){
				   h.postDelayed(this, 2000);
			   } else {
				   String[] tempArr = g.split(",");
				   String value = tempArr[0]  + "," + tempArr[1]  + "," + tempArr[2]  + "," + (Integer.parseInt(tempArr[3]) + 1) + "," + seeds;
				   doInBackground("put", ch1 + ":" + ch2, value);
				   h.removeCallbacks(this);
			   }
		   }
	};
	
	private Runnable updateScore = new Runnable(){
		   @Override
		   public void run() {
			  Log.d("Dabble polling","updating scores");
			  String gameStatus = doInBackground("get", ch1 + ":" + ch2);
			  /*
			    ch1 = keys[1];
				ch2 = keys[2]; ch1:ch2 = game id
				scores = keys[3]; <starts as a single score, add the other score before storing> score1:score2
				nums = keys[4]; <starts as a single num, add the other num before storing> num1:num2
			 */
			  
			  if (!(gameStatus.equals("Error: No Such Key")||gameStatus.equals("server unavailable"))){
				  	//add to current game status
				  	//challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)
				  String[] tempArr = gameStatus.split(",");
				  String[] oldScores = tempArr[1].split(":");
				  String[] oldNums = tempArr[2].split(":");
				  String gameSeriesNum = tempArr[3];
				  if (gameSeriesNum.equals("0") || tempArr[4].equals("none")){
					  Log.d("Dabble polling","trying to update scores before the game was initialized, check code");
				  } else {
					   String c1imei =  doInBackground("get", ch1);
					   String c2imei = doInBackground("get", ch2);
					   String value = ch1+":"+ch2; 
					   
					   if (imei.equals(c1imei)){ // we are the challenger
						   value += "," + scores + ":" + oldScores[1] //scores updated
								   + "," + nums + ":" + oldNums[1] //move numbers updated
									+ "," + gameSeriesNum //game series number remains the same
									+ "," + tempArr[4];
						   doInBackground("put", ch1 + ":" + ch2, value);
						   doInBackground("put", c2imei, "toast,"+ ch1 + " found: "+ nums +" word(s) and has a new score of: " + scores);
						   h.removeCallbacks(updateScore);
						   
					   } else if (imei.equals(c2imei)){ // we were challenged
						   value += "," + oldScores[0] + ":" + scores //scores updated
								   + "," + oldNums[0] + ":" + nums //move numbers updated
									+ "," + gameSeriesNum //game series number remains the same
									+ "," + tempArr[4];
						   
						   doInBackground("put", c1imei, ch2 + " has a new score of: " + scores);
						   doInBackground("put", ch1 + ":" + ch2, value);
						   h.removeCallbacks(updateScore);
						   
					   } else {
						   Log.d("Dabble polling", "unable to determine if we are the challenger or the challenged user");
						   h.postDelayed(this, 15000); //try again in 15 seconds
					   }
				  }
			  		h.removeCallbacks(updateScore);
			  		
			  } else if(gameStatus.equals("server unavailable")){
				  Log.d("Dabble polling", gameStatus + " while polling for: " + ch1 + ":" + ch2);
				  	h.postDelayed(this, 15000); //checks again in 15 seconds
			  } else {
				  Log.d("Dabble polling", ch1 + " did not have an imei registered");
			  }
	   }
		   
		  
	};
	
	private Runnable updateSeed = new Runnable(){
		   @Override
		   public void run() {
			   
			   String gameStatus = doInBackground("get", ch1 + ":" + ch2);
			   String[] tempArr = gameStatus.split(",");
			   if (gameStatus.equals("Error: No Such Key")){
				   Log.d("Dabble polling","updating the seed failed, key does not exist");
				   h.postDelayed(this, 15000);
			   } else if(gameStatus.equals("server unavailable")){
				   Log.d("Dabble polling","server unavailable while updating seed to " + seeds);
				   h.postDelayed(this, 15000);
			   } else {
				   
				 //challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)
				   //users scores num game# seed
				   
				   String value = tempArr[0];
				   value += "," + tempArr[1] + "," + tempArr[2] + "," + ((Integer.parseInt(tempArr[3]) + 1)) + seeds;
				   
				if (KeyValueAPI.isServerAvailable()){
					Log.d("Dabble polling","updating the seed");
					doInBackground("put", tempArr[0], value);
					h.removeCallbacks(this);
				} else {
					h.postDelayed(this, 15000);
				}
				   
			   }
		   }
	};
	
	

	private Runnable pollForNotification = new Runnable(){
		//start polling every 15 seconds to see if someone has challenged me to
		//a game using my imei (watch) as the key and the value will be the game's key
		//notes for self:(username:imei) -> (imei:gameID) -> (gameID:updates)
		   public void run() {
			  Log.d("Dabble polling","polling");
			  
			  if (imei == null){
				  TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
				  imei = telephonyManager.getDeviceId().toString();
			  }
			  
			  
			  String temp = doInBackground("get", imei);
			  if(temp.equals("0")||temp.equals("Error: No Such Key")){
				  
			  } else if(!temp.contains("toast")&&temp.contains(":")) {
				  String[] temp2 = temp.split(":");
				  if(temp2.length>=2){
				  postNotification(temp2[0],temp2[1]);
				  }
				  h.removeCallbacks(this);
			  } else if(temp.contains("toast")){
				  				  
					  String[] mess = temp.split(",");
					  toastMessage = mess[1];
					  h.post(toasty);
					  doInBackground("put", imei, "0");
				  
			  }
			  
		      h.postDelayed(this, 15000);
		   }
		};
		
		
	private Runnable alerter = new Runnable() {
		public void run() {
			alert.show();
		}
	};
			
	private Runnable toasty = new Runnable() {
		public void run() {
			Toast t = Toast.makeText(context, toastMessage, Toast.LENGTH_LONG);
			t.show();
		}
	};

}
