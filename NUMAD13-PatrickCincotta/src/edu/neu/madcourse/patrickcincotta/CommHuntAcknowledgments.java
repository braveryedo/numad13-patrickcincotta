package edu.neu.madcourse.patrickcincotta;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class CommHuntAcknowledgments extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comm_huntactivity_acknowledgments);
		System.out.println("Check");
		String ackText = "1) Salman Zaidi: http://stackoverflow.com/questions/14199671/scale-down-bitmap-from-resource-in-good-quality-and-memory-efficient" +
				", we used his decodeImage method as a base for ours." +
				"\n\n2) Juliet Kemp: http://myjeeva.com/how-to-convert-image-to-string-and-string-to-image-in-java.html" +
				", we used her code as a base for calling the camera, writing and getting the image from storage" +
				"\n\n3) Mirek Rusin: http://stackoverflow.com/questions/4139288/android-how-to-handle-right-to-left-swipe-gestures" +
				", we used his GestureListener class as is" +
				"\n\n4) Jeevanandam M: http://myjeeva.com/how-to-convert-image-to-string-and-string-to-image-in-java.html" +
				", we used his code to convert the image to and from a string in our old implementation. This code is no longer active";
		
		TextView tv = (TextView) findViewById(R.id.ackText);
		tv.setText(ackText);
	}


}
