package edu.neu.madcourse.patrickcincotta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DabblePauseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dabble_activity_pause);

		
		
	}

	
	public void cheaterButton(View v){
    	Intent intent = new Intent(this, DabbleHintActivity.class);
    	startActivity(intent);
	}
	
	public void resumeGameButton(View v){
		 finish();
	}
	
	public void toggleMusicButton(View v){
		 DabbleActivity.toggleMusicButton(v);
	}
	
	public void dabAckButton(View v){
		Intent intent = new Intent(this, DabbleAcknowledgements.class);
    	startActivity(intent);
	}

}
