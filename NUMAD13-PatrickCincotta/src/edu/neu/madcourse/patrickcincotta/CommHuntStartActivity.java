package edu.neu.madcourse.patrickcincotta;

import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.neu.mhealth.api.KeyValueAPI;

public class CommHuntStartActivity extends Activity {
	final String gameID = "001"; // Place-holder for this example
	Button commButton;
	Button conButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comm_huntactivity_start);
		
		commButton = (Button) findViewById(R.id.comStart);
		conButton = (Button) findViewById(R.id.conButton);
		conButton.setVisibility(View.GONE);
	}

	
	private class AsynchSendData extends AsyncTask<String, Void, String> {
	    
    	protected String doInBackground(String... params) {	    	
    		String object = params[0];
    		
    		int attempts = 0;		    		

    		System.out.println("Checking Internet Connection...");
    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
				attempts++;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
    			System.out.println("Pinging Server...");
			if (KeyValueAPI.isServerAvailable()){
				
				Calendar c = Calendar.getInstance();
				long secondsSinceEpoch = c.getTimeInMillis();
				String startTime = "" + secondsSinceEpoch;
				
				KeyValueAPI.put("chlacher", "qwer1234", "comHunt" + gameID + "startTime", startTime);
				KeyValueAPI.put("chlacher", "qwer1234", "comHunt" + gameID + "object", object);
				System.out.println("Success");	
		}
	}
    		return null;
    }

    @Override
    protected void onPostExecute(String result) {
    	Toast.makeText(getApplicationContext(), "Commercial Has Started!", Toast.LENGTH_LONG).show();
    	conButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
	
	public void onClickConf(View view){
		EditText et = (EditText) findViewById(R.id.searchObject);
		String object = et.getText().toString();
		new AsynchSendData().execute(object);
	}
	
	public void onClickCont(View view){
		new AsynchCheckTimeStamp().execute();
	}
	
	public void onClickAck(View view){
		Intent intent = new Intent(this, CommHuntAcknowledgments.class);
		startActivity(intent);
	}
	
	private class AsynchCheckTimeStamp extends AsyncTask<String, Void, String> {
	    
    	protected String doInBackground(String... params) {	    	
    		
    		int attempts = 0;		    		

    		System.out.println("Checking Internet Connection...");
    		ConnectivityManager conMang = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo activeNetworkInfo = conMang.getActiveNetworkInfo();

    		while(attempts < 50 && !(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
				attempts++;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
    		if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
    			System.out.println("Pinging Server...");
			if (KeyValueAPI.isServerAvailable()){
				Calendar c = Calendar.getInstance();
				long secondsSinceEpoch = c.getTimeInMillis();
				
				String serMins = KeyValueAPI.get("chlacher", "qwer1234", "comHunt" + gameID + "startTime");
				
				long timediff = secondsSinceEpoch - Long.valueOf(serMins);
				
				if (timediff <= 180000){
					return "y";
				} else {
					return "n";
				}
		}
	}
    		return null;
    }

    @Override
    protected void onPostExecute(String result) {
    	if (result != null && result.equals("y")){
    		Intent intent = new Intent(getApplicationContext(), CommHuntCameraActivity.class);
    		intent.putExtra("gameID", gameID);
    		startActivity(intent);
    	} else if (result != null) {
    		Toast.makeText(getApplicationContext(), "Commercial has Expired! Join within 3 minutes next time!", Toast.LENGTH_LONG).show();
    		conButton.setVisibility(View.GONE);
    	} else {
    		Toast.makeText(getApplicationContext(), "Connection Failed!", Toast.LENGTH_LONG).show();
    	}
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
}
