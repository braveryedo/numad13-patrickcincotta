package edu.neu.madcourse.patrickcincotta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
//import edu.neu.mhealth.apis.KeyValueAPI;



public class DabbleActivity extends Activity implements OnClickListener {
	
Random r;
ArrayList<String> letterArray;
static ArrayList<String> cheaterCheaterPumpkinEater;
String seedWords;
Button prevButton;
int prevIndex;
int numClicked;
Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16,b17,b18;
TextView timeText;
TextView scoreText;
InputStream istream;
boolean won;
int score;
int subscore1, subscore2;
Handler h;
int timeKeeper;
boolean new1,new2,new3,new4;
static boolean music;
static int musicId;
boolean challengeMode;
DabblePolling poll;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dabble_main_activity);
		
		music = true;
		r  = new Random();
		score = 0;
		won = false;
		prevButton = null;
		prevIndex = 0;
		numClicked = 0;
		subscore2 = 0;
		letterArray = new ArrayList<String>(
			Arrays.asList("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18")
		);
		cheaterCheaterPumpkinEater = new ArrayList<String>(
				Arrays.asList("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18")
		);
		
		
		
		
		
		//http://freepd.com/Chill/Chill%20Dark
		musicId = R.raw.chilldark;
		
		b1 = (Button) findViewById(R.id.dab_button1);
		b1.setOnClickListener(this);
		b2 = (Button) findViewById(R.id.dab_button2);
		b2.setOnClickListener(this);
		b3 = (Button) findViewById(R.id.dab_button3);
		b3.setOnClickListener(this);
		b4 = (Button) findViewById(R.id.dab_button4);
		b4.setOnClickListener(this);
		b5 = (Button) findViewById(R.id.dab_button5);
		b5.setOnClickListener(this);
		b6 = (Button) findViewById(R.id.dab_button6);
		b6.setOnClickListener(this);
		b7 = (Button) findViewById(R.id.dab_button7);
		b7.setOnClickListener(this);
		b8 = (Button) findViewById(R.id.dab_button8);
		b8.setOnClickListener(this);
		b9 = (Button) findViewById(R.id.dab_button9);
		b9.setOnClickListener(this);
		b10 = (Button) findViewById(R.id.dab_button10);
		b10.setOnClickListener(this);
		b11 = (Button) findViewById(R.id.dab_button11);
		b11.setOnClickListener(this);
		b12 = (Button) findViewById(R.id.dab_button12);
		b12.setOnClickListener(this);
		b13 = (Button) findViewById(R.id.dab_button13);
		b13.setOnClickListener(this);
		b14 = (Button) findViewById(R.id.dab_button14);
		b14.setOnClickListener(this);
		b15 = (Button) findViewById(R.id.dab_button15);
		b15.setOnClickListener(this);
		b16 = (Button) findViewById(R.id.dab_button16);
		b16.setOnClickListener(this);
		b17 = (Button) findViewById(R.id.dab_button17);
		b17.setOnClickListener(this);
		b18 = (Button) findViewById(R.id.dab_button18);
		b18.setOnClickListener(this);
		
		timeText = (TextView) findViewById(R.id.Time);
		scoreText = (TextView) findViewById(R.id.Score);	
		
		newGameButton(b1);
		
		setLettersToButtons();
		
		SudokuMusic.play(this, musicId);


	}
	
	
	private void setLettersToButtons() {
		b1.setText(letterArray.get(1));
		b2.setText(letterArray.get(2));
		b3.setText(letterArray.get(3));
		b4.setText(letterArray.get(4));
		b5.setText(letterArray.get(5));
		b6.setText(letterArray.get(6));
		b7.setText(letterArray.get(7));
		b8.setText(letterArray.get(8));
		b9.setText(letterArray.get(9));
		b10.setText(letterArray.get(10));
		b11.setText(letterArray.get(11));
		b12.setText(letterArray.get(12));
		b13.setText(letterArray.get(13));
		b14.setText(letterArray.get(14));
		b15.setText(letterArray.get(15));
		b16.setText(letterArray.get(16));
		b17.setText(letterArray.get(17));
		b18.setText(letterArray.get(18));
	}


	
	public void onClick(View v) {
		
		Log.d("Dabble button: ", "" + getResources().getResourceEntryName(v.getId()));
		numClicked++;
		Log.d("Dabble numclicked", "" + numClicked);
		v.playSoundEffect(SoundEffectConstants.CLICK);
		
		if(!won){
		
		switch (v.getId()){
		//I will be editing this later by removing common code from each case and putting it into another method
		//to improve readability (when I have time)
			case R.id.dab_button1:	
					if (numClicked == 2){
						
						Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
						prevIndex + " and " + 1);
						
						CharSequence b1Text = letterArray.get(prevIndex);
						CharSequence b2Text = letterArray.get(1);
						
						b1.setText(b1Text);
						prevButton.setText(b2Text);
						
						letterArray.set(1, b1Text.toString());
						letterArray.set(prevIndex, b2Text.toString());
						
						numClicked = 0;
						prevIndex = 0;	
						prevButton = null;
					} else {
						prevIndex = 1;
						prevButton = b1;
					}
					
					break;
			case R.id.dab_button2:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 2);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(2);
					
					b2.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(2, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 2;
					prevButton = b2;
				}
				break;
			case R.id.dab_button3:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 3);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(3);
					
					b3.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(3, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 3;
					prevButton = b3;
				}
				break;
				
			case R.id.dab_button4:
				if (numClicked == 2){
					

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 4);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(4);
					
					b4.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(4, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 4;
					prevButton = b4;
				}
				break;
			case R.id.dab_button5:
				if (numClicked == 2){

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 5);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(5);
					
					b5.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(5, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 5;
					prevButton = b5;
				}
				break;
			case R.id.dab_button6:
				if (numClicked == 2){

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 6);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(6);
					
					b6.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(6, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 6;
					prevButton = b6;
				}
				break;
			case R.id.dab_button7:
				if (numClicked == 2){

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 7);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(7);
					
					b7.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(7, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 7;
					prevButton = b7;
				}
				break;
			case R.id.dab_button8:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 8);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(8);
					
					b8.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(8, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 8;
					prevButton = b8;
				}
				break;
			case R.id.dab_button9:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 9);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(9);
					
					b9.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(9, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;
					prevButton = null;
					
				} else {
					prevIndex = 9;
					prevButton = b9;
				}
				break;
			case R.id.dab_button10:
				if (numClicked == 2){

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 10);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(10);
					
					b10.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(10, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 10;
					prevButton = b10;
				}
				break;
			case R.id.dab_button11:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 11);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(11);
					
					b11.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(11, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 11;
					prevButton = b11;
				}
				break;
			case R.id.dab_button12:
				if (numClicked == 2){

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 12);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(12);
					
					b12.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(12, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 12;
					prevButton = b12;
				}
				break;
			case R.id.dab_button13:
				if (numClicked == 2){

					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 13);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(13);
					
					b13.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(13, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 13;
					prevButton = b13;
				}
				break;
			case R.id.dab_button14:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 14);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(14);
					
					b14.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(14, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 14;
					prevButton = b14;
				}
				break;
			case R.id.dab_button15:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 15);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(15);
					
					b15.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(15, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 15;
					prevButton = b15;
				}
				break;
			case R.id.dab_button16:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 16);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(16);
					
					b16.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(16, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 16;
					prevButton = b16;
				}
				break;
			case R.id.dab_button17:
				if (numClicked == 2){
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 17);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(17);
					
					b17.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(17, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;
					prevButton = null;
					
				} else {
					prevIndex = 17;
					prevButton = b17;
				}
				break;
			case R.id.dab_button18:
				if (numClicked == 2){
					
					Log.d("Dabble activity, 2 buttons have been pressed: ", "swapping buttons " + 
					prevIndex + " and " + 18);
					
					CharSequence b1Text = letterArray.get(prevIndex);
					CharSequence b2Text = letterArray.get(18);
					
					b18.setText(b1Text);
					prevButton.setText(b2Text);
					
					letterArray.set(18, b1Text.toString());
					letterArray.set(prevIndex, b2Text.toString());
					
					numClicked = 0;
					prevIndex = 0;	
					prevButton = null;
					
				} else {
					prevIndex = 18;
					prevButton = b18;
				}
				break;
			
		}
		
		
		checkWords();
		
		} 
	}
	

	public void onPause() {
		h.removeCallbacks(runnable);
		SudokuMusic.stop(this);
		super.onPause();
		
	}
	
	public void onResume(){
		super.onResume();
		
		h.post(runnable);
		if(music){
		SudokuMusic.play(this, musicId);
		}
		
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId().toString();
		DabblePolling poll = new DabblePolling(this);
		
		poll.doInBackground("poll", imei);
	
		
	}
	
	public void pauseGameButton(View v){
		h.removeCallbacks(runnable);
		Log.d("Dabble: ", "pause button clicked");
		
    	Intent intent = new Intent(this, DabblePauseActivity.class);
    	startActivity(intent);

	}
	
	public void quitGameButton(View v){
		finish();
	}
	
	public static void toggleMusicButton(View v){
		music = !music;
		Log.d("Dabble : ", "music: " + music);
			if (!music){
				SudokuMusic.stop(null);
			}

	}
	



	public void newGameButton(View v) {
		
		new1 = true;
		new2 = true;
		new3 = true;
		new4 = true;
		
		Intent intents = getIntent();
		String cr = intents.getStringExtra("challenger");
		String cd = intents.getStringExtra("challenged");
		String challengeWords = "";
		String temp = "Error: no Such Key";
		challengeMode = false;
		if (cr != null && cd != null){
			
			poll = new DabblePolling(this);
			
			poll.doInBackground("removeNotification");
			temp = poll.doInBackground("get", cr + ":" + cd);
			Log.d("Dabble challengeWords:", temp +" is the gameStatus");
			//challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)
			challengeWords = temp.split(",")[4];
			Log.d("Dabble challengeWords:", challengeWords);
			
			Log.d("Dabble Activity", cd+" received challenge from " + cr );
			challengeMode = true;
		}
		
		
		if (challengeMode == false){
			
			Log.d("Dabble, newGame: ", "getting new words");
			String threeWord = getWord(3);
			Log.d("Dabble, newGame: ", "3: "+ threeWord);
			String fourWord = getWord(4);
			Log.d("Dabble, newGame: ", "4: "+ fourWord);
			String fiveWord = getWord(5);
			Log.d("Dabble, newGame: ", "5: "+ fiveWord);
			String sixWord = getWord(6);
			Log.d("Dabble, newGame: ", "6: "+ sixWord);
			
			String truffleShuffle = "" + threeWord + fourWord + fiveWord + sixWord;
			seedWords = truffleShuffle;
			
			letterArray.set(1, threeWord.substring(0,1));
			letterArray.set(2, threeWord.substring(1,2));
			letterArray.set(3, threeWord.substring(2,3));
			
			letterArray.set(4, fourWord.substring(0,1));
			letterArray.set(5, fourWord.substring(1,2));
			letterArray.set(6, fourWord.substring(2,3));
			letterArray.set(7, fourWord.substring(3,4));
			
			letterArray.set(8, fiveWord.substring(0,1));
			letterArray.set(9, fiveWord.substring(1,2));
			letterArray.set(10, fiveWord.substring(2,3));
			letterArray.set(11, fiveWord.substring(3,4));
			letterArray.set(12, fiveWord.substring(4,5));
			
			letterArray.set(13, sixWord.substring(0,1));
			letterArray.set(14, sixWord.substring(1,2));
			letterArray.set(15, sixWord.substring(2,3));
			letterArray.set(16, sixWord.substring(3,4));
			letterArray.set(17, sixWord.substring(4,5));
			letterArray.set(18, sixWord.substring(5,6));
			
			for (int i = 0; i<19; i++){
				cheaterCheaterPumpkinEater.set(i, letterArray.get(i)); 
				//Log.d("Dabble: ", "letterArry at " + i + " is " + letterArray.get(i));
			}
			
			
			shuffleLetters(truffleShuffle);		

			
		} else {
			
			if (cr != null && cd != null){
				
				if(challengeWords.equals("none")){
				
					Log.d("Dabble, newGame: ", "getting new words");
					String threeWord = getWord(3);
					Log.d("Dabble, newGame: ", "3: "+ threeWord);
					String fourWord = getWord(4);
					Log.d("Dabble, newGame: ", "4: "+ fourWord);
					String fiveWord = getWord(5);
					Log.d("Dabble, newGame: ", "5: "+ fiveWord);
					String sixWord = getWord(6);
					Log.d("Dabble, newGame: ", "6: "+ sixWord);
					
					String truffleShuffle = "" + threeWord + fourWord + fiveWord + sixWord;
					seedWords = truffleShuffle;
					
					//challenger:challenged,challengerScore:challengedScore,challengerWordsFound:challengedWordsFound,game#,word seeds("none" if game# is 0)
					poll.doInBackground("nextBoard", cr, cd, seedWords);
					
					
					
					letterArray.set(1, threeWord.substring(0,1));
					letterArray.set(2, threeWord.substring(1,2));
					letterArray.set(3, threeWord.substring(2,3));
					
					letterArray.set(4, fourWord.substring(0,1));
					letterArray.set(5, fourWord.substring(1,2));
					letterArray.set(6, fourWord.substring(2,3));
					letterArray.set(7, fourWord.substring(3,4));
					
					letterArray.set(8, fiveWord.substring(0,1));
					letterArray.set(9, fiveWord.substring(1,2));
					letterArray.set(10, fiveWord.substring(2,3));
					letterArray.set(11, fiveWord.substring(3,4));
					letterArray.set(12, fiveWord.substring(4,5));
					
					letterArray.set(13, sixWord.substring(0,1));
					letterArray.set(14, sixWord.substring(1,2));
					letterArray.set(15, sixWord.substring(2,3));
					letterArray.set(16, sixWord.substring(3,4));
					letterArray.set(17, sixWord.substring(4,5));
					letterArray.set(18, sixWord.substring(5,6));
					
					for (int i = 0; i<19; i++){
						cheaterCheaterPumpkinEater.set(i, letterArray.get(i)); 
						//Log.d("Dabble: ", "letterArry at " + i + " is " + letterArray.get(i));
					}
					
					
					
					shuffleLetters(truffleShuffle);		
				}
				
			} else {
				challengeMode = false;
			}
		}
		
		
		if (won){
			subscore2 = score;
		}
		
		won = false;
		
		setLettersToButtons();
			
		checkWords();
			
		Button NGB = (Button) findViewById(R.id.dab_newGame);
		NGB.setText("New Game");
		
		
		h = new Handler();

		timeKeeper = 240;
		h.post(runnable);
		

	}
	
	public void scoreScreen(View v){ 
		Intent intent = new Intent(this, DabbleScoreScreen.class);
		intent.putExtra("userScore", score);
		intent.putExtra("challengeMode", String.valueOf(challengeMode));
		startActivity(intent);
	}

	private Runnable runnable = new Runnable() {
		   @Override
		   public void run() {
			   timeText.setText("Time: " + timeKeeper/2);
			   score = subscore1 + timeKeeper + subscore2;
			   scoreText.setText("Score: " + (score));

			if (!won){
			   timeKeeper =  timeKeeper - 1;
			} else {
				Button NGB = (Button) findViewById(R.id.dab_newGame);
				NGB.setText("Continue");
			}
			   if(timeKeeper < 0){
				   timeKeeper = 0;
				   scoreScreen(null);			    	
			   }
			   
			   if(timeKeeper < 40){
				   timeText.setTextColor(Color.RED);
			   } else {
				   timeText.setTextColor(Color.BLACK);
			   }
		      h.postDelayed(this, 1000);
		   }
		};
		

	private void shuffleLetters(String s) {
		String local = s;
		Random rand = new Random();
		for (int i = 1; i<=18; i++){
			
			if(i==18){
				letterArray.set(i, local);
			} else {
				int ind = rand.nextInt(local.length()-1);
				
				letterArray.set(i, local.substring(ind, ind+1));
				
				StringBuilder sb = new StringBuilder(local);
				sb.deleteCharAt(ind);
				local = sb.toString();
			}
		}
		
	}


	private String getWord(int i) {
		
		String word = null;
		int q = r.nextInt(178691); // number of lines in the total wordlist.txt file
		
		try{
			istream = this.getAssets().open("wordlist.txt");
			Log.d("Dabble, getWord: ", "istream open");
			BufferedReader buff = new BufferedReader(new InputStreamReader(istream));
			String line = "";
			line = buff.readLine();
			
			
			for(int count = 0; count < q; count++){
				line = buff.readLine();
			}
			
			while (line != null){
				
				if(line.length() == i){
					word = line;
					return word;
				}
				line = buff.readLine();	
			}
			
			if(word == null){
				return getWord(i);
			}
			
			} catch (IOException e) {
				Log.d("Dabble, getWord: ", e.toString());
			}
		
		return getWord(i);
	}


	private void checkWords() {
		
		CharSequence row1 = "" + b1.getText() + b2.getText() + b3.getText();
		
		CharSequence row2 = "" + b4.getText() + b5.getText() + b6.getText() + b7.getText();
				
		CharSequence row3 = "" + b8.getText() + b9.getText() + b10.getText() + b11.getText() + b12.getText();
		
		CharSequence row4 = "" + b13.getText() + b14.getText() + b15.getText() + b16.getText() + b17.getText() + b18.getText();
		
		int row1Color = Color.BLACK;
		int row2Color = Color.BLACK;
		int row3Color = Color.BLACK;
		int row4Color = Color.BLACK;
		
		subscore1 = 0;
		boolean results1 = checkWord(row1);
		boolean results2 = checkWord(row2);
		boolean results3 = checkWord(row3);
		boolean results4 = checkWord(row4);
		
		
		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);

		
		
	
		if (results1){
			row1Color = Color.RED;
			subscore1 += 50;
			
			if(new1){
				if(music)
				r.play();
				new1 = false;
			}
		} else {
			new1 = true;
		}
		if (results2){
			row2Color = Color.RED;
			subscore1 += 100;
			
			if(new2){
				if(music)
				r.play();
				
				new2 = false;
			}
		} else {
			new2 = true;
		}
		if (results3){
			row3Color = Color.RED;
			subscore1 += 175;
			if(new3){
				if(music)
				r.play();
				new3 = false;
			}
		} else {
			new3 = true;
		}
		if (results4){
			row4Color = Color.RED;
			subscore1 += 275;
			if(new4){
				if(music)
				r.play();
				new4 = false;
			}
		} else {
			new4 = true;
		}		
		
		if(challengeMode){

			//challengeCount represents the number of words that have been completed
			int challengeCount = 0;
			if(new1){
				challengeCount++;
			}
			if(new2){
				challengeCount++;
			}
			if(new3){
				challengeCount++;
			}
			if(new4){
				challengeCount++;
			}
			
			if (challengeCount > 0){
				Intent intents = getIntent();
				String cr = intents.getStringExtra("challenger");
				String cd = intents.getStringExtra("challenged");
				poll.doInBackground("notify", cr, cd, ""+score, ""+challengeCount,seedWords);
				
			}
			
		}
		
			b1.setTextColor(row1Color);
			b2.setTextColor(row1Color);
			b3.setTextColor(row1Color);

			b4.setTextColor(row2Color);
			b5.setTextColor(row2Color);
			b6.setTextColor(row2Color);
			b7.setTextColor(row2Color);

			b8.setTextColor(row3Color);
			b9.setTextColor(row3Color);
			b10.setTextColor(row3Color);
			b11.setTextColor(row3Color);
			b12.setTextColor(row3Color);

			b13.setTextColor(row4Color);
			b14.setTextColor(row4Color);
			b15.setTextColor(row4Color);
			b16.setTextColor(row4Color);
			b17.setTextColor(row4Color);
			b18	.setTextColor(row4Color);		
		
			if (results1&&results2&&results3&&results4){
				won = true;
				subscore1 += 300;
				Log.d("Dabble","you are the winrar");
			}
		
		
	}


	private boolean checkWord(CharSequence s) {
		String q = s.toString();
		String firstChar = q.substring(0,1);
		Boolean result = false;
		String fileName = firstChar + "words.txt";

			Log.d("Dabble, checkWord: ", "about to open istream");
			
			try{
			istream = this.getAssets().open(fileName);
			Log.d("Dabble, checkWord: ", "istream open");
			BufferedReader buff = new BufferedReader(new InputStreamReader(istream));
			String line = "";
			line = buff.readLine();
			
			
			while (line != null){
				int comp = line.compareToIgnoreCase(q);
				
				//Log.d("Dabble, checkWord: ", "comparing " + q + " to " + line);
				
				if(comp == 0){
					result = true;
					Log.d("Dabble, checkWord: ", "word found " + q);
					buff.close();
					istream.close();
					break;
				} else if(comp > 0){
					Log.d("Dabble, checkWord: ", "word not found " + q);
					buff.close();
					istream.close();
					break;
				}
				line = buff.readLine();
				
			}
			
			
			} catch (IOException e) {
				Log.d("Dabble, checkWord: ", e.toString());
			}
		
		return result;
	}
	
	
	

}
